#!/bin/bash
echo
echo Welcome to lisca project creator! 🐟

read -p 'Enter the project name: ' projectName 

mv ProjectName $projectName
testSuffix="Tests"
mv ProjectNameTests $projectName$testSuffix

sed -i '' "s/___ProjectName___/$projectName/" README.md
sed -i '' "s/___ProjectName___/$projectName/" project.yml
sed -i '' "s/___ProjectName___/$projectName/" swiftgen.yml
sed -i '' "s/___ProjectName___/$projectName/" fastlane/Matchfile

read -p 'Enter the bundle id: ' bundleId 

sed -i '' "s/___bundleid___/$bundleId/" project.yml
sed -i '' "s/___bundleid___/$bundleId/" fastlane/Appfile
sed -i '' "s/___bundleid___/$bundleId/" fastlane/Matchfile

bundle install
xcodegen generate

echo Done! ✅
