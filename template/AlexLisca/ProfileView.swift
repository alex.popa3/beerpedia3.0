//
//  ProfileView.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//


import SwiftUI

struct ProfileView: View {
    @Environment(\.dependencies) private var dependencies: AppDependencies
    @EnvironmentObject var router: Router<AppRoute>

    @State private var username: String = ""
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var gender = "Male"
    private var genders = ["Male", "Female", "Other"]
    @State private var enableFlag: Bool = false

    var body: some View {
        VStack {
            Text(username)
            Form {
                Image("anmol")
                    .resizable()
                    .clipped()
                    .frame(width: 100, height: 100, alignment: .center)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.blue, lineWidth: 2.0))
                Section(header: Text("First Name")) {
                    TextField("First Name", text: $firstName)
                }
                Section(header: Text("Last Name")) {
                    TextField("Last Name", text: $lastName)
                }
                Section(header: Text("Additional")) {
                    Picker("Gender", selection: $gender) {
                        ForEach(genders, id: \.self) {
                            Text($0)
                        }
                    }
                    Toggle(isOn: $enableFlag) {
                        Text("Blogger")
                    }
                }
            }
            .background(.white)
            .background(Color.red)
            Button {
                print("Logout")
            } label: {
                Text("Logout")
            }
            .buttonStyle(Primary())
        }
        .padding()
      /*  .onReceive(dependencies.appState.updates(for: \.userData.state)) { userState in
            switch userState {
            case .undefined:
                break
            case let .logged(user):
                username = user.username ?? ""
            case .notLogged:
                router.setRootNode(.login)
            }
        }*/
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
