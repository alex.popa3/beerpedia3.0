//
//  FavoritesView.swift
//  AlexLisca
//
//  Created by Rawfish on 10/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Combine
import SwiftUI

struct FavoritesView: View {
    @Environment(\.dependencies) private var dependencies: AppDependencies
    @EnvironmentObject var router: Router<AppRoute>
    
    @State private var data: Loadable<[Beer]>
    
    init(data: Loadable<[Beer]> = .notRequested) {
        _data = .init(initialValue: data)
    }
    
    var body: some View {
        VStack {
            content
        }
        .onLoad {
            loadData()
        }
    }
        
    private var content: AnyView {
        switch data {
        case .notRequested:
            return AnyView(notRequestedView)
        case .isLoading:
            return AnyView(loadingView)
        case let .loaded(details):
            return AnyView(loadedView(data: details))
        case let .failed(error):
            return AnyView(failedView(error))
        }
    }
}

private extension FavoritesView {
    var notRequestedView: some View {
        VStack {
            Text("No data")
            Image(systemName: "star")
                .resizable()
                .frame(width: 100,height: 100)
        }
        .background(.red)
        .onAppear { print("no data")}
    }
    
    var loadingView: some View {
      
        VStack {
            ActivityIndicatorView()
            Button(action: { self.data.cancelLoading() },
                   label: { Text("common.cancel") })
        }
        .onAppear {  print("loading view")}
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            self.loadData()
        })
    }
    
    func loadedView(data: [Beer]) -> some View {
      
        ScrollView {
            LazyVStack {
                ForEach(data, id: \.self) { beer in
                    Button { router.push(.details(beer, dependencies.interactors.favoritesInteractor.isFavorite(id: String(beer.id)), false)) } label: {
                        BeerCell(beer: beer, isFavorite: dependencies.interactors.favoritesInteractor.isFavorite(id: String(beer.id)))
                    }
                    .buttonStyle(.plain)
                }
            }
           
        } .onReceive(dependencies.appState.map(\.favoriteIds)) { favorites in
            if favorites.isEmpty == true {
                router.dismiss()
                router.dismiss()
            }
        }
    }
}

private extension FavoritesView {
    func loadData() {
        dependencies.interactors.beerInteractor.updateFavorite(beers: $data)
    }
}

#if DEBUG
    struct FavoritesView_Previews: PreviewProvider {
        static var previews: some View {
            FavoritesView(data: .notRequested)
                .inject(.preview)
        }
    }
#endif
