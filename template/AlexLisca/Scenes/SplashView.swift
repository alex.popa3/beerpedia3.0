//
//  FirstView.swift
//  NewRoutingDemo
//
//  Created by Davide Pagin on 13/10/22.
//

import SwiftUI

struct SplashView: View {
    @EnvironmentObject var router: Router<AppRoute>

    var body: some View {
        VStack {
            Text("Welcome to splash screen")
            Button {
                router.setRootNode(.main)
            } label: {
                Text("Switch to main view")
            }
            Button {
                router.push(.main)
            } label: {
                Text("Push main view")
            }
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
