//
//  ListView.swift
//  AlexLisca
//
//  Created by Rawfish on 05/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Combine
import SwiftUI

struct ListView: View {
    @Environment(\.dependencies) private var dependencies: AppDependencies
    @EnvironmentObject var router: Router<AppRoute>

    @State private var data: Loadable<[Beer]>
    @State private var tabIndex = 0
    

    init(data: Loadable<[Beer]> = .notRequested) {
        _data = .init(initialValue: data)
       
    }

    var body: some View {
        content
    }

    private var content: AnyView {
        switch data {
        case .notRequested:
            return AnyView(notRequestedView)
        case let .isLoading(last):
            return AnyView(loadingView(cached: last))
        case let .loaded(details):
            return AnyView(loadedView(details))
        case let .failed(error):
            return AnyView(failedView(error))
        }
    }
}

private extension ListView {
    var notRequestedView: some View {
        Text("").onAppear {
            print("On loading data...")

            self.loadData(offset: data.value?.count ?? 0)
        }
    }

    func loadingView(cached: [Beer]?) -> some View {
        if let cached = cached {
            return AnyView(loadedView(cached))
        } else {
            return AnyView(ActivityIndicatorView().padding())
        }
    }

    func failedView(_ error: Error) -> some View {
        ErrorView(error: error,
                  retryAction: {
                      self.loadData(offset: data.value?.count ?? 0)
                  })
    }

    func loadedView(_ data: [Beer]) -> some View {
        InfiniteList(data: $data,
                     loadMore: {
                         self.loadData(offset: data.count)
                     },
                     content: { beer in
            Button { router.push(.details(beer, dependencies.interactors.favoritesInteractor.isFavorite(id: String(beer.id)), false)) } label: {
                             BeerCell(beer: beer, isFavorite: dependencies.interactors.favoritesInteractor.isFavorite(id: String(beer.id)))
                         }
           
                         .buttonStyle(.plain)

                     })

                     .refreshable { refreshData() }
    }
}

// MARK: - Side Effects

private extension ListView {
    func loadData(offset: Int) {
        dependencies.interactors.beerInteractor.load(beers: $data, offset: offset)
    }


    func refreshData() {
        dependencies.interactors.beerInteractor.reload(beers: $data)
    }
}

// MARK: - Preview

#if DEBUG
    struct ListView_Previews: PreviewProvider {
        static var previews: some View {
            ListView(data: .loaded(Beer.mockedData))
                .inject(.preview)
        }
    }
#endif
