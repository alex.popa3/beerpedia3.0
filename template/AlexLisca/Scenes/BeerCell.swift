//
//  BeerCell.swift
//  AlexLisca
//
//  Created by Rawfish on 08/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import SwiftUI

struct BeerCell: View {
    @Environment(\.dependencies) private var dependencies: AppDependencies
    let beer: Beer
    @State var isFavorite: Bool

    
    var body: some View {
        HStack(alignment: .top) {
            Spacer()
            VStack {
                AsyncImage(
                    url: URL(string: beer.image_url ?? "no image"),
                    content: { image in
                        image.resizable()
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .shadow(radius: 10)
                    },
                    placeholder: {
                        if beer.image_url != "no Image" && beer.image_url != nil {
                            ProgressView()
                        } else {
                            VStack {
                                Image(systemName: "photo.on.rectangle")
                                    .resizable()
                                    .frame(maxWidth: 50, maxHeight: 45)
                                    .padding(.bottom, 10)
                                Text("No image to load")
                                    .font(.system(size: 11))
                            }
                            .opacity(0.75)
                            .shadow(radius: 40)
                        }
                    }
                )
               
            }
            .frame(height: 150)
                .padding(10)
            
            Spacer()
            
            HStack(alignment: .center) {
                VStack(alignment: .leading, spacing: 10.0) {
                    Text(beer.name)
                        .lineLimit(1)
                        .font(.callout)
                       // .fontWeight(.semibold)
                    
                    Text(beer.description)
                        .font(.caption)
                        .lineLimit(2)
                    
                    if isFavorite {
                        HStack {
                            Image(systemName:  "star.fill" )
                                .foregroundColor(.yellow)
                        }
                    }
                }
            }
            .offset(x: 10, y: 20.0)
            Spacer()
        }
        .onReceive(dependencies.appState.map(\.favoriteIds)) { favorites in
            isFavorite = favorites.contains(where: { $0 == String(beer.id) })
        }
        .frame(maxHeight: 170)
        .padding(.all, 15)
        .background(.regularMaterial)
        .cornerRadius(40)
        .padding(.all)
        .shadow(color: .black.opacity(0.50), radius: 1.5)
    }
}
