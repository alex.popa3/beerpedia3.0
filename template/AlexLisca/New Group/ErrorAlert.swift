//
//  ErrorAlert.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import SwiftUI


struct ErrorAlert<A: View>: ViewModifier {
    var title: String
    @Binding var error: Error?
    var message: ((Error) -> String)? = nil
    var actions: (Error) -> A

    func body(content: Content) -> some View {
        content
            .alert(title, isPresented: Binding<Bool>(get: {
                error != nil
            }, set: { newValue in
                if !newValue {
                    error = nil
                }

            }), presenting: error, actions: { error in
                actions(error)
            }, message: { error in
                if let message = message?(error) {
                    Text(message)
                } else {
                    if let error = error as? LocalizedError {
                        Text(error.errorDescription ?? error.localizedDescription)
                    } else {
                        Text(error.localizedDescription)
                    }
                }

            })
    }
}

extension View {
    func errorAlert<A: View>(title: String, error: Binding<Error?>, message: ((Error) -> String)? = nil, @ViewBuilder actions: @escaping ((Error) -> A) = { _ in EmptyView() }) -> some View {
        modifier(ErrorAlert<A>(title: title, error: error, message: message, actions: actions))
    }

    func errorAlert<T, A: View>(title: String, state: LoadableSubject<T>, message: ((Error) -> String)? = nil, @ViewBuilder actions: @escaping ((Error) -> A) = { _ in EmptyView() }) -> some View {
        modifier(ErrorAlert<A>(title: title,
                               error: Binding<Error?>(get: {
                                   state.wrappedValue.error
                               }, set: { _, _ in

                               }), message: message, actions: actions))
    }
}

struct ErrorAlert_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            Text("Where is the Alert?")
                .errorAlert(title: "Test",
                            error: .constant(SessionError.cancelledByUser),
                            message: { _ in
                                "C'è stato un errore"
                            }, actions: { _ in
                                Button("Button 1", role: .none) {}
                                Button("Button 2", role: .none) {}
                                Button("Button 3", role: .none) {}
                                Button("Button 4", role: .destructive) {}
                                Button("Button 5", role: .destructive) {}
                                Button("Button 6", role: .destructive) {}
                                Button("Button Cancel", role: .cancel) {}
                            })
        }
    }
}
