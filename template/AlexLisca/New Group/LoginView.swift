//
//  LoginView.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//
import SwiftUI

struct LoginView: View {
    @EnvironmentObject var router: Router<AppRoute>
    @Environment(\.dependencies) private var dependencies: AppDependencies

    @State private var email: String = ""
    @State private var password: String = ""

    @State private var loginResult: Loadable<UserState> = .notRequested

    var body: some View {
        VStack {
            Form {
                Section(header: Text("Credenziali")) {
                    TextField("Email", text: $email)
                    SecureField("Password", text: $password)
                }
            }.frame(height: 132)
                .clearListBackground()
            HStack {
                Text("Password dimenticata")
                Spacer()
                Button {
                  //  router.push(.resetPassword)
                } label: {
                    Text("Reset password")
                        .foregroundColor(.black)
                        .bold()
                }
            }
            .padding()
            Group {
                Button {
                    router.push(.main)
                   /* dependencies.interactors.userInteractor.signIn(
                        credential: .email(email: email, password: password),
                        inViewController: nil,
                        result: $loginResult*/
                    
                } label: {
                    Text("Accedi")
                }
                .buttonStyle(Primary())
                Text("Oppure")
                    .padding()
                Button {
                   /* dependencies.interactors.userInteractor.signIn(
                        credential: .facebook(),
                        inViewController: UIApplication.shared.keyWindow?.rootViewController,
                        result: $loginResult
                    )*/
                } label: {
                    Label("Continua con Facebook", systemImage: "paperplane.fill")
                }
                .buttonStyle(Primary())
                .padding(.bottom, 16)
                Button {
                    /*
                    dependencies.interactors.userInteractor.signIn(
                        credential: .google(),
                        inViewController: UIApplication.shared.keyWindow?.rootViewController,
                        result: $loginResult
                    )*/
                } label: {
                    Label("Continua con Google", systemImage: "paperplane.fill")
                }
                .buttonStyle(Secondary())
                .padding(.bottom, 16)
                Button {/*
                    dependencies.interactors.userInteractor.signIn(
                        credential: .apple(),
                        inViewController: UIApplication.shared.keyWindow?.rootViewController,
                        result: $loginResult
                    )*/
                } label: {
                    Label("Continua con Apple", systemImage: "apple.logo")
                }
                .buttonStyle(Tertiary())
                Spacer()
            }
            .padding(.horizontal)
            Spacer()
                .frame(height: 32)
            HStack {
                Spacer()
                Text("Non hai un account?")
                Button {
                   // router.push(.signup)
                } label: {
                    Text("Registrati")
                        .foregroundColor(.black)
                        .bold()
                }
                Spacer()
            }
            Spacer()
        }
        .background(Color.lightGrey)
        .navigationBarTitle("Accedi")
        /*.onReceive(dependencies.appState.updates(for: \.userData.state).dropFirst(),
                   perform: { state in
                       print("state \(state)")
                       switch state {
                       case .undefined:
                           break
                       case .logged:
                           router.setRootNode(.splash)
                       case .notLogged:
                           break
                       }
                   })*/
        .errorAlert(title: "Login error",
                    state: $loginResult,
                    message: { error in
                        "C'è stato un errore: \(error.localizedDescription)"
                    },
                    actions: { _ in
                        Button("OK", role: .cancel) {}
                    })
        //.loadingOverlay(state: $loginResult, text: "Logging in...")
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LoginView()
        }
    }
}

struct ClearListBackgroundModifier: ViewModifier {
    func body(content: Content) -> some View {
        if #available(iOS 16.0, *) {
            content.scrollContentBackground(.hidden)
        } else {
            content
        }
    }
}

extension View {
    func clearListBackground() -> some View {
        modifier(ClearListBackgroundModifier())
    }
}
