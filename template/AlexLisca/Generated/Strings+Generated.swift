// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return prefer_self_in_static_references

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum Common {
    /// Allow Push
    internal static let allowPush = L10n.tr("Localizable", "common.allowPush", fallback: "Allow Push")
    /// Localizable.strings
    ///   lisca-swiftui
    /// 
    ///   Created by Alessandro Maroso on 18/01/23.
    internal static let cancel = L10n.tr("Localizable", "common.cancel", fallback: "Cancel")
  }
  internal enum Details {
    /// Ability
    internal static let ability = L10n.tr("Localizable", "details.ability", fallback: "Ability")
    /// Basic Info
    internal static let info = L10n.tr("Localizable", "details.info", fallback: "Basic Info")
    /// Name
    internal static let name = L10n.tr("Localizable", "details.name", fallback: "Name")
  }
  internal enum List {
    /// Pokemon List
    internal static let title = L10n.tr("Localizable", "list.title", fallback: "Pokemon List")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg..., fallback value: String) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
