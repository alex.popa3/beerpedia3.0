import Foundation
import SwiftUI

public extension View {
    @available(iOS, deprecated: 16.0, message: "Use SwiftUI's Navigation API beyond iOS 15")
    func nbNavigationDestination<D: Hashable>(for pathElementType: D.Type, @ViewBuilder destination builder: @escaping (D) -> some View) -> some View {
        modifier(DestinationBuilderModifier(typedDestinationBuilder: { AnyView(builder($0)) }))
    }
}

typealias DestinationBuilder<T> = (T) -> AnyView

struct DestinationBuilderModifier<TypedData>: ViewModifier {
    let typedDestinationBuilder: DestinationBuilder<TypedData>

    @EnvironmentObject var destinationBuilder: DestinationBuilderHolder

    func body(content: Content) -> some View {
        destinationBuilder.appendBuilder(typedDestinationBuilder)

        return content
            .environmentObject(destinationBuilder)
    }
}

class DestinationBuilderHolder: ObservableObject {
    static func identifier(for type: Any.Type) -> String {
        String(reflecting: type)
    }

    var builders: [String: (Any) -> AnyView?] = [:]

    init() {
        builders = [:]
    }

    func appendBuilder<T>(_ builder: @escaping (T) -> AnyView) {
        let key = Self.identifier(for: T.self)
        builders[key] = { data in
            if let typedData = data as? T {
                return builder(typedData)
            } else {
                return nil
            }
        }
    }

    func build(_ typedData: some Any) -> AnyView {
        let base = (typedData as? AnyHashable)?.base
        let type = type(of: base ?? typedData)
        let key = Self.identifier(for: type)

        if let builder = builders[key], let output = builder(typedData) {
            return output
        }
        assertionFailure("No view builder found for key \(key)")
        return AnyView(Image(systemName: "exclamationmark.triangle"))
    }
}

struct DestinationBuilderView<Data>: View {
    let data: Data

    @EnvironmentObject var destinationBuilder: DestinationBuilderHolder

    var body: some View {
        destinationBuilder.build(data)
    }
}
