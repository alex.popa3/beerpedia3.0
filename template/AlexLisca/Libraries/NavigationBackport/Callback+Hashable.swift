//
//  HashableCallback.swift
//  NewRoutingDemo
//
//  Created by Alessandro Maroso on 14/12/22.
//

import Foundation

struct Callback<V, T>: Hashable {
    private let uuid: UUID
    static func == (lhs: Callback<V, T>, rhs: Callback<V, T>) -> Bool {
        lhs.uuid == rhs.uuid
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid.hashValue)
    }

    let callBack: (V) -> T

    init(_ callback: @escaping (V) -> T) {
        uuid = UUID()
        callBack = callback
    }
}

class CallbackObject: HashableObject {
    @Published var called = false
}

class HashableObject: ObservableObject, Hashable {
    private var uuid = UUID()
    static func == (lhs: HashableObject, rhs: HashableObject) -> Bool {
        lhs.uuid == rhs.uuid
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid.hashValue)
    }
}
