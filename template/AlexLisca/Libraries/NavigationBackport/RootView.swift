//
//  RootView.swift
//  NewRoutingDemo
//
//  Created by Alessandro Maroso on 13/12/22.
//

import SwiftUI

struct RootView<Node, Content>: View where Node: Hashable & Identifiable, Content: View {
    @StateObject private var routing: Router<Node>
    private let content: (Node) -> Content
    let rootTransition: AnyTransition

    init(rootNode: Node, rootTransition: AnyTransition = .opacity, @ViewBuilder content: @escaping (Node) -> Content) {
        _routing = StateObject(wrappedValue: Router(rootNode: rootNode))
        self.content = content
        self.rootTransition = rootTransition
    }

    var body: some View {
        NBNavigationStack(path: $routing.path) {
            ZStack {
                content(routing.rootNode)
                    .transition(rootTransition)
            }.nbNavigationDestination(for: Node.self) { node in
                content(node)
            }
        }
        .sheet(item: $routing.presentedNode, content: content)
        .fullScreenCover(item: $routing.fullScreenPresentedNode, content: content)
        .environmentObject(routing)
    }
}
