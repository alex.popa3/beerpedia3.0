//
//  Routing.swift
//  NewRoutingDemo
//
//  Created by Davide Pagin on 19/10/22.
//

import Foundation
import SwiftUI
import UIKit

class Router<Node: Hashable>: ObservableObject {
    private var stack: [Node]

    init(rootNode: Node) {
        stack = [rootNode]
    }

    var rootNode: Node {
        stack.first!
    }

    @Published var path = NBNavigationPath() {
        didSet {
            let removedCount = (stack.count - 2) - path.count
            if removedCount > 0 {
                stack.removeLast(removedCount)
            }
        }
    }

    @Published var presentedNode: Node? = nil
    @Published var fullScreenPresentedNode: Node? = nil

    func push(_ navigationNode: Node) {
        path.append(navigationNode)
        stack.append(navigationNode)
    }

    func pop() {
        path.removeLast(1)
    }

    func popToRoot() {
        setRootNode(rootNode)
    }

    func popTo(_ node: Node) {
        if let lastIndex = stack.lastIndex(of: node) {
            withAnimation {
                path.removeLast(stack.count - 1 - lastIndex)
            }
        }
    }

    func popToNode(where condition: (Node) -> Bool) {
        if let lastIndex = stack.lastIndex(where: { condition($0) }) {
            withAnimation {
                path.removeLast(stack.count - 1 - lastIndex)
            }
        }
    }

    func setChildNode(_ childNode: Node, forNode parentNode: Node) {
        if let parentIndex = stack.firstIndex(of: parentNode) {
            let removedCount = stack.count - 1 - parentIndex
            stack.removeSubrange(parentIndex + 1 ... (stack.count - 1))
            withAnimation {
                path.removeLast(removedCount - 1)
                push(childNode)
            }
        }
    }

    func setChildNode(_ childNode: Node, forNodeWhere condition: (Node) -> Bool) {
        if let parentIndex = stack.firstIndex(where: { condition($0) }) {
            let removedCount = stack.count - 1 - parentIndex
            stack.removeSubrange(parentIndex + 1 ... (stack.count - 1))
            withAnimation {
                path.removeLast(removedCount - 1)
                push(childNode)
            }
        }
    }

    func setNode(stack: [Node]) {
        UINavigationBar.setAnimationsEnabled(false)
        popToRoot()
        Task {
            await scheduleRemainingSteps(steps: stack)
        }
    }

    @MainActor
    private func scheduleRemainingSteps(steps: [Node]) async {
        guard let firstStep = steps.first else {
            UINavigationBar.setAnimationsEnabled(true)
            return
        }
        path.append(firstStep)
        stack.append(firstStep)
        do {
            try await Task.sleep(nanoseconds: UInt64(0.05 * 1_000_000_000))
            await scheduleRemainingSteps(steps: Array(steps.dropFirst()))
        } catch {}
    }

    func setRootNode(_ node: Node, animated: Bool = true) {
        withAnimation(animated ? .default : nil) {
            stack = [node]
            path = NBNavigationPath()
        }
    }

    func present(_ node: Node, fullScreen: Bool) {
        if fullScreen {
            fullScreenPresentedNode = node
        } else {
            presentedNode = node
        }
    }

    func dismiss() {
        if presentedNode != nil || fullScreenPresentedNode != nil {
            presentedNode = nil
            fullScreenPresentedNode = nil
        } else {
            pop()
        }
    }
}
