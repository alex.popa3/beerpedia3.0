func apply<T>(_ transform: (inout T) -> Void, to input: T) -> T {
    var transformed = input
    transform(&transformed)
    return transformed
}
