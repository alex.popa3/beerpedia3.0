//
//  Plist.swift
//  lisca
//
//  Created by Pietro Basso on 19/12/2018.
//  Copyright © 2018 Rawfish. All rights reserved.
//

import Foundation

/// A class to read and decode strongly typed values in `.plist` files.
///
/// You need to specify a `Codable` structure which can handle your interesting data.
/// Then you can allocate the `InfoPlist` struct with the generic type `Plist`.
///
/// ## Example
///
/// Your `Info.plist` file contains an additional node called `configuration` with several data inside (`baseUrl`, `headers`, etc.).
///
///
///     struct InfoPlist: Codable {
///         let configuration: Configuration
///
///         struct Configuration: Codable {
///             let baseUrl: URL?
///             let headers: [String: String]
///             let api: String
///         }
///     }
///
/// Now you can access your data with `Plist`.
///
///     guard let plist = try? Plist<InfoPlist>() else { return }
///     let baseUrl = plist.data.configuration.baseUrl // URL?
///     let headers = plist.data.configuration.headers // [String: String] dictionary
///
public struct Plist<Value: Codable> {
    /// Errors.
    ///
    /// - fileNotFound: plist file not exists.
    public enum Errors: Error {
        case fileNotFound
    }

    /// Plist file source.
    ///
    /// - infoPlist: main bundel's Info.plist file
    /// - plist: other plist file with custom name
    public enum Source {
        case infoPlist(_: Bundle)
        case plist(resource: String, bundle: Bundle)

        /// Get the raw data inside given plist file.
        ///
        /// - Returns: read data
        /// - Throws: throw an exception if it fails
        internal func data() throws -> Data {
            switch self {
            case let .infoPlist(bundle):
                guard let infoDict = bundle.infoDictionary else {
                    throw Errors.fileNotFound
                }
                return try JSONSerialization.data(withJSONObject: infoDict)
            case let .plist(resource, bundle):
                guard let path = bundle.path(forResource: resource, ofType: "plist") else {
                    throw Errors.fileNotFound
                }
                return try Data(contentsOf: URL(fileURLWithPath: path))
            }
        }
    }

    /// Data read for file
    public let data: Value

    /// Initialize a new Plist parser with given codable structure.
    ///
    /// - Parameter file: source of the plist
    /// - Throws: throw an exception if read fails
    public init(_ file: Plist.Source = .infoPlist(Bundle.main)) throws {
        let rawData = try file.data()
        let decoder = JSONDecoder()
        data = try decoder.decode(Value.self, from: rawData)
    }
}
