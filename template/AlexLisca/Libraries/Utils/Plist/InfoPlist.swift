//
//  InfoPlist.swift
//  Trya
//
//  Created by Ciprian Cojan on 22/11/2019.
//  Copyright © 2019 rawfish. All rights reserved.
//

import Foundation

public struct InfoPlist: Codable {
    let configuration: Configuration

    struct Configuration: Codable {
        let networking: Networking
    }
    
    struct Networking: Codable {
        let baseUrl: String
    }
}


extension Bundle {
    
    var appName: String {
        return object(forInfoDictionaryKey: "CFBundleName") as! String
       }
    var displayName: String {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
    }
       
       var appVersion: String {
           return object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
       }
       
       var buildNumber: String {
           return object(forInfoDictionaryKey: "CFBundleVersion") as! String
       }
}
