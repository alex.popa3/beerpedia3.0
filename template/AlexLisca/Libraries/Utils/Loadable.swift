//
//  Loadable.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 29/04/2020.
//  Copyright © 2020 Rawfish. All rights reserved.
//

import Combine
import Foundation
import SwiftUI

typealias LoadableSubject<Value> = Binding<Loadable<Value>>

enum Loadable<T> {
    case notRequested
    case isLoading(last: T?)
    case loaded(T)
    case failed(Error)

    var value: T? {
        switch self {
        case let .loaded(value): return value
        case let .isLoading(last): return last
        default: return nil
        }
    }

    var error: Error? {
        switch self {
        case let .failed(error): return error
        default: return nil
        }
    }
}

extension Loadable {
    mutating func setIsLoading() {
        self = .isLoading(last: value)
    }

    mutating func cancelLoading() {
        switch self {
        case let .isLoading(last):
            if let last {
                self = .loaded(last)
            } else {
                let error = NSError(
                    domain: NSCocoaErrorDomain, code: NSUserCancelledError,
                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Canceled by user",
                                                                            comment: "")]
                )
                self = .failed(error)
            }
        default: break
        }
    }

    func map<V>(_ transform: (T) throws -> V) -> Loadable<V> {
        do {
            switch self {
            case .notRequested: return .notRequested
            case let .failed(error): return .failed(error)
            case let .isLoading(value):
                return try .isLoading(last: value.map { try transform($0) })
            case let .loaded(value):
                return try .loaded(transform(value))
            }
        } catch {
            return .failed(error)
        }
    }
}

protocol SomeOptional {
    associatedtype Wrapped
    func unwrap() throws -> Wrapped
}

struct ValueIsMissingError: Error {
    var localizedDescription: String {
        NSLocalizedString("Data is missing", comment: "")
    }
}

extension Optional: SomeOptional {
    func unwrap() throws -> Wrapped {
        switch self {
        case let .some(value): return value
        case .none: throw ValueIsMissingError()
        }
    }
}

extension Loadable where T: SomeOptional {
    func unwrap() -> Loadable<T.Wrapped> {
        map { try $0.unwrap() }
    }
}

extension Loadable: CustomStringConvertible {
    var description: String {
        switch self {
        case .notRequested:
            return "Not request"
        case let .isLoading(last):
            return "Loading with value \(String(describing: last))"
        case let .loaded(t):
            return "Loaded with value: \(t)"
        case let .failed(error):
            return "Failed with error \(error)"
        }
    }
}

extension Loadable: Equatable where T: Equatable {
    static func == (lhs: Loadable<T>, rhs: Loadable<T>) -> Bool {
        switch (lhs, rhs) {
        case (.notRequested, .notRequested): return true
        case let (.isLoading(lhsV), .isLoading(rhsV)): return lhsV == rhsV
        case let (.loaded(lhsV), .loaded(rhsV)): return lhsV == rhsV
        case let (.failed(lhsE), .failed(rhsE)):
            return lhsE.localizedDescription == rhsE.localizedDescription
        default: return false
        }
    }
}

extension LoadableSubject {
    func networkBoundResource<T>(
        query: (() async throws -> T?)?,
        shouldFetch: (T?) async -> (Bool) = { _ in true },
        fetch: (T?) async throws -> T,
        saveFetch: ((T) async throws -> Void)?
    ) async where Value == Loadable<T> {
        do {
            let cached = try await query?()

            if await (shouldFetch(cached)) {
                wrappedValue = .isLoading(last: cached)
                let data = try await fetch(cached)
                wrappedValue = .loaded(data)
                try await saveFetch?(data)
            } else {
                if let cached {
                    wrappedValue = .loaded(cached)
                }
            }

        } catch {
            wrappedValue = .failed(error)
        }
    }
}
