//
//  CoreDataHelpers.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 29/04/2020.
//  Copyright © 2020 Rawfish. All rights reserved.
//

import CoreData

// MARK: - ManagedEntity

protocol ManagedEntity: NSFetchRequestResult {}

extension ManagedEntity where Self: NSManagedObject {
    static var entityName: String {
        String(describing: Self.self)
    }

    static func insertNew(in context: NSManagedObjectContext) -> Self? {
        NSEntityDescription
            .insertNewObject(forEntityName: entityName, into: context) as? Self
    }

    static func newFetchRequest() -> NSFetchRequest<Self> {
        .init(entityName: entityName)
    }
}

// MARK: - NSManagedObjectContext

extension NSManagedObjectContext {
    func configureAsReadOnlyContext() {
        automaticallyMergesChangesFromParent = true
        mergePolicy = NSRollbackMergePolicy
        undoManager = nil
        shouldDeleteInaccessibleFaults = true
    }

    func configureAsUpdateContext() {
        mergePolicy = NSOverwriteMergePolicy
        undoManager = nil
    }
}

// MARK: - Misc

extension NSSet {
    func toArray<T>(of _: T.Type) -> [T] {
        allObjects.compactMap { $0 as? T }
    }
}
