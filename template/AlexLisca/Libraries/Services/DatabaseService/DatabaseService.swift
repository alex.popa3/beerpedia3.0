//
//  DatabaseService.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 10/12/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import CoreData

protocol DatabaseService {
    typealias DatabaseOperation<R> = (NSManagedObjectContext) throws -> R

    func count<E>(_ fetchRequest: NSFetchRequest<E>) async throws -> Int
    func fetch<E, R>(_ fetchRequest: NSFetchRequest<E>,
                     map: @escaping (E) throws -> R?) async throws -> [R]
    func update<R>(_ operation: @escaping DatabaseOperation<R>) async throws -> R
}

class CoreDataService: DatabaseService {
    private let container: NSPersistentContainer

    init(directory: FileManager.SearchPathDirectory = .documentDirectory,
         domainMask: FileManager.SearchPathDomainMask = .userDomainMask,
         version vNumber: UInt) {
        let version = Version(vNumber)
        container = NSPersistentContainer(name: version.modelName)
        if let url = version.dbFileURL(directory, domainMask) {
            let store = NSPersistentStoreDescription(url: url)
            container.persistentStoreDescriptions = [store]
        }
        container.loadPersistentStores(completionHandler: { [weak container] _, _ in
            container?.viewContext.configureAsReadOnlyContext()

        })
       
    }

    func count(_ fetchRequest: NSFetchRequest<some Any>) async throws -> Int {
        try await container.viewContext.perform { [weak container] in
            try container?.viewContext.count(for: fetchRequest) ?? 0
        }
    }

    func fetch<E, R>(_ fetchRequest: NSFetchRequest<E>,
                     map: @escaping (E) throws -> R?) async throws -> [R] {
        try await container.viewContext.perform { [weak container] in
            try container?.viewContext.fetch(fetchRequest).compactMap { try map($0) } ?? []
        }
    }

    func update<R>(_ operation: @escaping DatabaseOperation<R>) async throws -> R {
        let context = container.newBackgroundContext()
        context.configureAsUpdateContext()
        return try await context.perform {
            let result = try operation(context)
            if context.hasChanges {
                try context.save()
            }
            context.reset()
            return result
        }
    }
}

// MARK: - Versioning

extension CoreDataService.Version {
    static var actual: UInt { 1 }
}

extension CoreDataService {
    struct Version {
        private let number: UInt

        init(_ number: UInt) {
            self.number = number
        }

        var modelName: String {
            "db_model_v1"
        }

        func dbFileURL(_ directory: FileManager.SearchPathDirectory,
                       _ domainMask: FileManager.SearchPathDomainMask) -> URL? {
            FileManager.default
                .urls(for: directory, in: domainMask).first?
                .appendingPathComponent(subpathToDB)
        }

        private var subpathToDB: String {
            "db.sql"
        }
    }
}
