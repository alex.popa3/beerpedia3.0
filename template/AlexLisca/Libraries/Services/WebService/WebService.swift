//
//  WebService.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 07/12/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import Foundation

protocol WebService {
    var session: URLSession { get }
    var decoder: JSONDecoder { get }
    var baseURL: String { get }
}

extension WebService {
    func call<Value>(endpoint: APIRequest, httpCodes: HTTPCodes = .success) async throws -> Value
        where Value: Decodable {
        debugPrint("\(endpoint.method) \(endpoint.path)")
        let request = try endpoint.urlRequest(baseURL: baseURL)
        let response = try await session.data(for: request)
        guard let code = (response.1 as? HTTPURLResponse)?.statusCode else {
            throw APIError.unexpectedResponse
        }
        guard httpCodes.contains(code) else {
            throw APIError.httpCode(code)
        }
        let data = response.0
        return try decoder.decode(Value.self, from: data)
           
    }
}

protocol APIRequest {
    var path: String { get }
    var method: String { get }
    var headers: [String: String]? { get }
    func body() throws -> Data?
}

enum APIError: Swift.Error {
    case invalidURL
    case httpCode(HTTPCode)
    case unexpectedResponse
    case imageProcessing([URLRequest])
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return "Invalid URL"
        case let .httpCode(code):
            return "Unexpected HTTP code: \(code)"
        case .unexpectedResponse:
            return "Unexpected response from the server"
        case .imageProcessing:
            return "Unable to load image"
        }
    }
}

extension APIRequest {
    func urlRequest(baseURL: String) throws -> URLRequest {
        guard let url = URL(string: baseURL + path) else {
            throw APIError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod = method
       request.allHTTPHeaderFields = headers
        request.httpBody = try body()
        return request
    }
}

typealias HTTPCode = Int
typealias HTTPCodes = Range<HTTPCode>

extension HTTPCodes {
    static let success = 200 ..< 300
}
