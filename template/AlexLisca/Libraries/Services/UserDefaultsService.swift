//
//  UserDefaultsService.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 16/03/23.
//

import Foundation

protocol UserDefaultsService {
    func value<T>(for key: UserDefaultsKey) -> T?
    func set<T>(value: T, for key: UserDefaultsKey)
    func hasValue(for key: UserDefaultsKey) -> Bool
    func removeValue(for key: UserDefaultsKey)
}

public enum UserDefaultsKey: String {
    case shouldShowWizard
    case favoritesIds
}

final class StandardUserDefaultsService: UserDefaultsService {
    func value<T>(for key: UserDefaultsKey) -> T? {
        UserDefaults.standard.object(forKey: key.rawValue) as? T ?? nil
    }

    func set(value: some Any, for key: UserDefaultsKey) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }

    func hasValue(for key: UserDefaultsKey) -> Bool {
        UserDefaults.standard.object(forKey: key.rawValue) != nil
    }

    func removeValue(for key: UserDefaultsKey) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
}
