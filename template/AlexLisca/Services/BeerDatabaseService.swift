//
//  PokemonDatabaseServices.swift
//  AlexLisca
//
//  Created by Rawfish on 04/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import CoreData

protocol BeerDatabaseService {
    func hasLoadedBeers() async throws -> Bool
    func store(beer: [Beer]) async throws
    func beer(with id: String)  async throws -> Beer?
    func beers() async throws -> [Beer]
    func details(for beerId: String) async throws -> Beer.Details?
    func store(beer details: Beer.Details) async throws
    func clearBeers() async throws
}

class BeerCoreDataService: CoreDataService, BeerDatabaseService {
    
    func hasLoadedBeers() async throws -> Bool {
        let fetchRequest = BeerManagedEntity.getBeer()
        let count = try await count(fetchRequest)
        return count > 0 
    }
    
  
    
   func beer(with id: String) async throws -> Beer? {
        let fetchRequest = BeerManagedEntity.newFetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@",id)
        return try await fetch(fetchRequest) {
            Beer(managedObject: $0)
        }.first
    }
    
    func beers() async throws -> [Beer] {
        let fetchRequest = BeerManagedEntity.newFetchRequest()
        fetchRequest.predicate = NSPredicate(value: true)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        return try await fetch(fetchRequest) {
            Beer(managedObject: $0)
        }
    }
    
    func store(beer: [Beer]) async throws {
        try await update { context in
            beer.forEach {
                $0.store(in: context)
            }
        }
    }
    
    func details(for beerId: String) async throws -> Beer.Details? {
        let fetchRequest = BeerDetailsManagedEntity.newFetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", beerId)
        fetchRequest.fetchLimit = 1
        let result = try await fetch(fetchRequest) {
            Beer.Details(managedObject: $0)
        }
        return result.first
    }
    
    func store(beer details: Beer.Details) async throws {
        _ = try await update { context in
            details.store(in: context)
        }
    }
    
    func clearBeers() async throws {
        let request = BeerManagedEntity.fetchRequest()
        return try await update { context in
            if let entities = try? context.fetch(request) {
                for entity in entities {
                    context.delete(entity)
                }
            }
        }
    }
}

extension BeerManagedEntity {
    static func getBeer() -> NSFetchRequest<BeerManagedEntity> {
        let request = newFetchRequest()
        request.fetchLimit = 1
        return request
    }
}
