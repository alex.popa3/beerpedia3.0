//
//  BeerWebService.swift
//  AlexLisca
//
//  Created by Rawfish on 04/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation

protocol BeerWebService: WebService {
    func getBeers(page: Int) async throws -> [Beer]
    func getDetails(with id: Int) async throws -> [Beer.Details]
    func getRandomBeer() async throws -> [Beer.Details]
}

class RealBeerWeebService: BeerWebService {
    let session: URLSession
    let decoder: JSONDecoder = .init()
    let baseURL: String
    
    init(session: URLSession,
         baseUrl: String)
    {
        self.session = session
        self.baseURL = baseUrl
    }
    
    func getBeers(page: Int) async throws -> [Beer] {
        try await call(endpoint: Request(endpoint: .allBeers(page)))
    }
    
    func getDetails(with id: Int) async throws -> [Beer.Details] {
        try await call(endpoint: Request(endpoint: .beer(String(id))))
    }
    
    func getRandomBeer() async throws -> [Beer.Details] {
        try await call(endpoint: Request(endpoint: .randomBeer))
    }
    
}

extension RealBeerWeebService {
    enum Endpoint {
        case allBeers(Int)
        case beer(String)
        case beerDetails(String)
        case randomBeer
    }
    
    struct Request: APIRequest {
        var headers: [String: String]?
        
        let endpoint: Endpoint
        
        var path: String {
            switch endpoint {
            case let .allBeers(offset):
                return "beers?page=\(offset)&per_page=80"
            case let .beer(id):
                return "beers/\(id)"
            case let .beerDetails(path):
                return "beers/\(path)"
            case .randomBeer:
                return "beers/random"
                //\(Int.random(in: 1...325))
            }
        }
        
        var method: String {
            switch endpoint {
            case .allBeers, .beer, .beerDetails, .randomBeer:
                return "GET"
            }
        }
        
        func body() throws -> Data? {
            nil
        }
    }
}
