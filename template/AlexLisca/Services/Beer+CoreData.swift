//
//  Beer+CoreData.swift
//  AlexLisca
//
//  Created by Rawfish on 04/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import CoreData

extension BeerManagedEntity: ManagedEntity {}

extension Beer {
    @discardableResult
    func store(in context: NSManagedObjectContext) -> BeerManagedEntity? {
        guard let beer = BeerManagedEntity.insertNew(in: context)
        else { return nil }
        beer.id = Int16(id) ?? 0
        beer.name = name
        beer.beerDescription = description
        beer.imageUrl = image_url ?? "no Image"
        return beer
    }

    init?(managedObject: BeerManagedEntity) {
        guard let name = managedObject.name,
              let image = managedObject.imageUrl,
              let description = managedObject.beerDescription
        else { return nil }
        let id = Int(managedObject.id)

        self.init(id: id, image_url: image, name: name, description: description, details: nil)
    }
}

extension BeerDetailsManagedEntity: ManagedEntity {}

extension Beer.Details {
    @discardableResult
    func store(in context: NSManagedObjectContext) -> BeerDetailsManagedEntity? {
        guard let details = BeerDetailsManagedEntity.insertNew(in: context) else { return nil }

        details.id = "\(id)"
        details.name = name
        details.imageUrl = image_url
        details.beerDescription = description
        details.abv = abv ?? 0.0
        details.attenuationLevel = attenuation_level ?? 0.0
        details.brewerTips = brewers_tips
        details.contributedBy = contributed_by
        details.ebc = ebc ?? 0.0
        details.firstBrewed = first_brewed
        details.foodPairings = food_pairing
        details.ibu = Double(ibu ?? 0)
        details.ph = ph ?? 0.0
        details.srm = srm ?? 0.0
        details.tagline = tagline
        details.targetOg = target_og ?? 0.0
        details.targetFg = target_fg ?? 0.0
        details.volume = volume.store(in: context)
        details.boilVolume = boil_volume.store(in: context)
        details.method = method.store(in: context)
        print("STORING DATA")
        print(details.method)
        print("ENDED STORE METHOD DATA")
        details.ingredients = ingredients.store(in: context)
        details.foodPairings = food_pairing
        details.brewerTips = brewers_tips
        details.contributedBy = contributed_by

        return details
    }

    init?(managedObject: BeerDetailsManagedEntity) {
        guard let id = managedObject.id,
              let name = managedObject.name,
              let image = managedObject.imageUrl,
              let description = managedObject.beerDescription,
              let brewerTips = managedObject.brewerTips,
              let contributedBy = managedObject.contributedBy,
              let firstBrewed = managedObject.firstBrewed,
              let foodPairings = managedObject.foodPairings,
              let tagline = managedObject.tagline
        else {
            return nil
        }
        let abv = managedObject.abv
        let attenuationLevel = managedObject.attenuationLevel
        let ebc = managedObject.ebc
        let ibu = managedObject.ibu
        let ph = managedObject.ph
        let srm = managedObject.srm
        let targetFg = managedObject.targetFg

        let targetOg = managedObject.targetOg
        var volume: Beer.UnitOfMeasure {
            let storeVolume = managedObject.volume

            return Beer.UnitOfMeasure(value: storeVolume?.value, unit: storeVolume?.unit)
        }
        var boilVolume: Beer.UnitOfMeasure {
            let storedBoilVolume = managedObject.boilVolume
            return Beer.UnitOfMeasure(value: storedBoilVolume?.value, unit: storedBoilVolume?.unit)
        }
        var ingredients: Beer.Details.Ingredients {
            let ingredients = managedObject.ingredients
            let hops = (managedObject.ingredients?.hops ?? NSSet())
                .toArray(of: HopManagedEntity.self)
                .compactMap { Beer.Details.Ingredients.Hop(managedObject: $0) }
            let malts = (managedObject.ingredients?.malts ?? NSSet())
                .toArray(of: MaltManagedEntity.self)
                .compactMap { Beer.Details.Ingredients.Malt(managedObject: $0) }

            return Beer.Details.Ingredients(malt: malts, hops: hops, yeast: ingredients?.yeast)
        }
        var method: Beer.Details.Method {
           
            let mashTemp = (managedObject.method?.mashTemp ?? NSSet())
                .toArray(of: MashTempManagedEntity.self)
                .compactMap { Beer.Details.Method.Mash_temp(managedObject: $0) }
            let storedFermentation = managedObject.method?.fermentation
            let fermentation = Beer.UnitOfMeasure(value: storedFermentation?.value, unit: storedFermentation?.unit)
            let twist = managedObject.method?.twist

            return Beer.Details.Method(mash_temp: mashTemp, fermentation: fermentation, twist: twist)
        }
        
        print("OFFLINE INIT DATA ")
        print(method)

        self.init(id: Int(id) ?? 0, name: name, tagline: tagline, first_brewed: firstBrewed, description: description, image_url: image, abv: abv, ibu: ibu, target_fg: targetFg, target_og: targetOg, ebc: ebc, srm: srm, ph: ph, attenuation_level: attenuationLevel, volume: volume, boil_volume: boilVolume, method: method, ingredients: ingredients, food_pairing: foodPairings, brewers_tips: brewerTips, contributed_by: contributedBy)
    }
}

extension IngredientsManagedEntity: ManagedEntity {}

extension Beer.Details.Ingredients {
    @discardableResult
    func store(in context: NSManagedObjectContext) -> IngredientsManagedEntity? {
        guard let ingredient = IngredientsManagedEntity.insertNew(in: context) else { return nil }
        print(malt)
        let storedMalt = malt.compactMap { $0.store(in: context) }
        ingredient.malts = NSSet(array: storedMalt)
        let storedHop = hops.compactMap { $0.store(in: context) }
        ingredient.hops = NSSet(array: storedHop)
        ingredient.yeast = yeast
        return ingredient
    }

    init?(managedObject: IngredientsManagedEntity) {
        guard let yeast = managedObject.yeast else { return nil }
        let storedMalts = (managedObject.malts ?? NSSet())
            .toArray(of: MaltManagedEntity.self)
            .compactMap { Beer.Details.Ingredients.Malt(managedObject: $0) }
        let storedHop = (managedObject.hops ?? NSSet())
            .toArray(of: HopManagedEntity.self)
            .compactMap { Beer.Details.Ingredients.Hop(managedObject: $0) }

        self.init(malt: storedMalts, hops: storedHop, yeast: yeast)
    }
}

extension MaltManagedEntity: ManagedEntity {}

extension Beer.Details.Ingredients.Malt {
    @discardableResult
    func store(in context: NSManagedObjectContext) -> MaltManagedEntity? {
        guard let malt = MaltManagedEntity.insertNew(in: context) else { return nil }
        malt.name = name
        let storedAmount = amount.store(in: context)
        malt.amount = storedAmount
        return malt
    }

    init?(managedObject: MaltManagedEntity) {
        guard let name = managedObject.name,
              let value = managedObject.amount?.value,
              let unit = managedObject.amount?.unit
        else {
            return nil
        }

        self.init(name: name, amount: Beer.UnitOfMeasure(value: value, unit: unit))
    }
}

extension HopManagedEntity: ManagedEntity {}

extension Beer.Details.Ingredients.Hop {
    @discardableResult
    func store(in context: NSManagedObjectContext) -> HopManagedEntity? {
        guard let hop = HopManagedEntity.insertNew(in: context) else { return nil }
        hop.name = name
        hop.add = add
        hop.attribute = attribute
        let storedAmount = amount.store(in: context)
        hop.amount = storedAmount

        return hop
    }

    init?(managedObject: HopManagedEntity) {
        guard let name = managedObject.name,
              let add = managedObject.add,
              let attribute = managedObject.attribute,
              let amountValue = managedObject.amount?.value,
              let amountUnit = managedObject.amount?.unit
        else { return nil }
        self.init(name: name, amount: Beer.UnitOfMeasure(value: amountValue, unit: amountUnit), add: add, attribute: attribute)
    }
}

extension MethodManagedEntity: ManagedEntity {}

extension Beer.Details.Method {
    func store(in context: NSManagedObjectContext) -> MethodManagedEntity? {
        guard let method = MethodManagedEntity.insertNew(in: context) else { return nil }
        method.fermentation = fermentation.store(in: context)
        let storedMashTemp = mash_temp.compactMap { $0.store(in: context) }
        method.mashTemp = NSSet(array: storedMashTemp)
        print("I VALORI STORATI DI METHOD-MASH-TEMP\(method.mashTemp)")
        return method
    }

    init?(managedObject: MethodManagedEntity) {
        
        let fermentationValue = managedObject.fermentation?.value
              let fermentationUnit = managedObject.fermentation?.unit
              let twist = managedObject.twist
        let mashTemps = (managedObject.mashTemp ?? NSSet())
            .toArray(of: MashTempManagedEntity.self)
            .compactMap { Beer.Details.Method.Mash_temp(managedObject: $0) }
        print("Dato preso dall'init\(mashTemps)")
        self.init(mash_temp: mashTemps, fermentation: Beer.UnitOfMeasure(value: fermentationValue, unit: fermentationUnit), twist: twist)
    }
}

extension MashTempManagedEntity: ManagedEntity {}

extension Beer.Details.Method.Mash_temp {
    func store(in context: NSManagedObjectContext) -> MashTempManagedEntity? {
        guard let mashTemp = MashTempManagedEntity.insertNew(in: context) else { return nil }
        mashTemp.duration = duration ?? 0.0
        mashTemp.temp = temp.store(in: context)
        return mashTemp
    }

    init?(managedObject: MashTempManagedEntity) {
        guard let mashTempValue = managedObject.temp?.value,
              let mashTempUnit = managedObject.temp?.unit
        else {
            return nil
        }
        let duration = managedObject.duration

        self.init(temp: Beer.UnitOfMeasure(value: mashTempValue, unit: mashTempUnit), duration: duration)
    }
}

extension UnitOfMeasureManagedEntity: ManagedEntity {}

extension Beer.UnitOfMeasure {
    @discardableResult
    func store(in context: NSManagedObjectContext) -> UnitOfMeasureManagedEntity? {
        guard let measure = UnitOfMeasureManagedEntity.insertNew(in: context) else { return nil }
        measure.unit = unit
        measure.value = value ?? 0.0
        return measure
    }

    init?(managedObject: UnitOfMeasureManagedEntity) {
        guard let unit = managedObject.unit else {
            return nil
        }
        let value = managedObject.value
        self.init(value: value, unit: unit)
    }
}
