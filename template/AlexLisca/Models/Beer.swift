//
//  Beer.swift
//  AlexLisca
//
//  Created by Rawfish on 04/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation

struct Beer: Codable, Equatable, Identifiable, Hashable {
    let id: Int
    let name: String
    let image_url: String?
    let description: String
    let details: Details? = nil

    init(id: Int, image_url: String?, name: String, description: String, details: Details?) {
        self.image_url = image_url
        self.name = name
        self.description = description

        self.id = id
    }

    struct Details: Codable, Equatable, Identifiable, Hashable {
        let id: Int
        let name: String
        let tagline: String
        let first_brewed: String
        let description: String
        let image_url: String?
        let abv: Double?
        let ibu: Double?
        let target_fg: Double?
        let target_og: Double?
        let ebc: Double?
        let srm: Double?
        let ph: Double?
        let attenuation_level: Double?
        let volume: UnitOfMeasure
        let boil_volume: UnitOfMeasure
        let method: Method
        let ingredients: Ingredients
        let food_pairing: [String]?
        let brewers_tips: String?
        let contributed_by: String?

        func hash(into hasher: inout Hasher) {
            hasher.combine(self.id)
        }

        struct Method: Codable, Equatable, Identifiable {
            let mash_temp: [Mash_temp]
            let fermentation: UnitOfMeasure
            let twist: String?

            var id: String? { self.fermentation.unit! }

            // MARK: - MashTemp

            struct Mash_temp: Codable, Equatable, Identifiable,Hashable {
                let temp: UnitOfMeasure
                let duration: Double?

                var id: String? { self.temp.unit! }
            }
        }

        typealias Id = Int

        struct Ingredients: Codable, Equatable, Identifiable,Hashable {
            let malt: [Malt]
            let hops: [Hop]
            let yeast: String?

            var id: String? { self.yeast! }

            struct Hop: Codable, Equatable, Identifiable,Hashable {
                var name: String?
                let amount: UnitOfMeasure
                let add: String?
                let attribute: String?

                var id: String? { self.name! }
            }

            // MARK: - Malt

            struct Malt: Codable, Equatable, Identifiable,Hashable {
                let name: String?
                let amount: UnitOfMeasure

                var id: String? { self.name! }
            }
        }
    }

    struct UnitOfMeasure: Codable, Equatable, Identifiable,Hashable {
        let value: Double?
        let unit: String?

        var id: String? { self.unit! }
    }
}

#if DEBUG

extension Beer {
    static let mockedData: [Beer] = [
        Beer(id: 1, image_url: nil, name: "Buzz", description: "A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.", details: nil),
       
    ]
}

#endif

#if DEBUG

extension Beer.Details {
    static let mockedData: [Beer.Details] = [
        Beer.Details(id: 1, name: "Buzz", tagline: "A real bitter experience", first_brewed: "09/2007", description: "A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.", image_url: 
                    nil, abv: 4.5, ibu: 60.0, target_fg: 1010, target_og: 1044, ebc: 20, srm: 10, ph: 4.4, attenuation_level: 75, volume: Beer.UnitOfMeasure(value: 20, unit: "litres"), boil_volume: Beer.UnitOfMeasure(value: 25, unit: "litres"), method: Beer.Details.Method(mash_temp: [Method.Mash_temp(temp: Beer.UnitOfMeasure(value: 64, unit: "celsius"), duration: 75)], fermentation: Beer.UnitOfMeasure(value: 19, unit: "celsius"), twist: ""), ingredients: Beer.Details.Ingredients(malt: [Ingredients.Malt(name: "marris ", amount: Beer.UnitOfMeasure(value: 3.3, unit: "kilograms"))], hops: [Ingredients.Hop(name: "ciao",amount: Beer.UnitOfMeasure(value: 0.4, unit:"kilograms"), add: "start", attribute: "later")], yeast: "Wyeast 1056 - American Ale™"), food_pairing: ["spicy chicken","salad","burritos"], brewers_tips: "The earthy and floral aromas from the hops can be overpowering. Drop a little Cascade in at the end of the boil to lift the profile with a bit of citrus.", contributed_by: "Sam Mason <samjbmason>")]
    
}

#endif
