//
//  Filter.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation

struct Filter: Hashable, Identifiable {
    struct Option: Hashable, Identifiable {
        let id: Int
        let name: String
        var isSelected: Bool
    }

    let id: Int
    let name: String
    var values: [Option]

    static let initialValues: [Filter] = [.init(id: 0,
                                                name: "Filter 1",
                                                values: [
                                                    .init(id: 0, name: "Option 1", isSelected: false),
                                                    .init(id: 1, name: "Option 2", isSelected: false),
                                                    .init(id: 2, name: "Option 3", isSelected: false)
                                                ]),
                                          .init(id: 1,
                                                name: "Filter 2",
                                                values: [
                                                    .init(id: 0, name: "Option 1", isSelected: false),
                                                    .init(id: 1, name: "Option 2", isSelected: false),
                                                    .init(id: 2, name: "Option 3", isSelected: false),
                                                    .init(id: 3, name: "Option 4", isSelected: false),
                                                    .init(id: 4, name: "Option 5", isSelected: false)
                                                ])]
}
