//
//  AppRoute.swift
//  AlexLisca
//
//  Created by Rawfish on 05/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation
import SwiftUI

enum AppRoute: Hashable, Identifiable {
    var id: Int {
        hashValue
    }
    case listView
    case profileView
    case splash
    case main
    case details(Beer,Bool,Bool)
    case detailSheet(Beer.Details,Bool,Bool,Bool)
    case favorites
    case loginView
    case filters

    @ViewBuilder
    func getView() -> some View {
        switch self {
        case .splash:
            SplashView()
        case .main:
           MainView()
        case let .details(Beer,favorite,random):
            DetailsView(beer: Beer.id, isFavorites: favorite,isRandom: random)
        case let .detailSheet(beer,method,ingredients,values):
            DetailsSheet(beer: beer,method: method,ingredients: ingredients,values: values)
        case let .favorites:
            FavoritesView()
        case let .listView:
            ListView()
        case let .profileView:
            ProfileView()
        case let .filters:
            FiltersView()
        case let .loginView:
            LoginView()
        }
    }
}
