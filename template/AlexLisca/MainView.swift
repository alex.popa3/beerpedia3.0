//
//  MainView.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Combine
import SwiftUI

struct MainView: View {
    @Environment(\.dependencies) private var dependencies: AppDependencies
    @EnvironmentObject var router: Router<AppRoute>

    @State private var data: Loadable<Void>
    @State private var random: Loadable<Beer>
    @State private var currentTabIndex = 0
  
    init(data: Loadable<Void> = .loaded(()), random: Loadable<Beer> = .notRequested) {
        _data = .init(initialValue: data)
        _random = .init(initialValue: random)
    }

    var body: some View {
        VStack {
            TabView(selection: $currentTabIndex) {
                ListView()
                    .tabItem {
                        Label("List", systemImage: "list.dash")
                    }
                    .tag(0)
                Text("Search")
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass")
                    }
                    .tag(1)
                FavoritesView()
                    .tabItem {
                        Label("Favorites", systemImage: "hand.thumbsup")
                    }
                    .tag(2)

                ProfileView()
                    .tabItem {
                        Label("Profile", systemImage: "person")
                    }
                
                    .tag(3)
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack {
                        Button(action: { router.present(.filters, fullScreen: false) },
                               label: { Image(systemName: "slider.horizontal.3") })
                        Button {
                            router.push(.details(.mockedData[0], false, true))
                        } label: {
                            Image(systemName: "dice.fill")
                        }
                    }
                    .opacity(currentTabIndex != 0 ? 0 : 1)
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle(Bundle.main.displayName)
        }
    }
}

// MARK: - Side Effects

private extension MainView {
    func loadData() {}
    
}

// MARK: - Preview

#if DEBUG
    struct MainView_Previews: PreviewProvider {
        static var previews: some View {
            MainView(data: .loaded(()))
                .inject(.preview)
        }
    }
#endif
