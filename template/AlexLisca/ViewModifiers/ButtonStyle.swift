//
//  ButtonStyle.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 16/03/23.
//  Copyright © 2022 RAWFISH SRL. All rights reserved.
//

import SwiftUI

struct Primary: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(maxWidth: .infinity, minHeight: 48)
            .frame(height: 48)
            .background(configuration.isPressed ? .black.opacity(0.6) : .black)
            .contentShape(Rectangle())
            .font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: 18))
            .foregroundColor(configuration.isPressed ? .white.opacity(0.6) : .white)
    }
}

struct Secondary: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(maxWidth: .infinity, minHeight: 48)
            .frame(height: 48)
            .background(configuration.isPressed ? Color.grey.opacity(0.6) : Color.grey)
            .contentShape(Rectangle())
            .font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: 18))
            .foregroundColor(configuration.isPressed ? .dirtyWhite.opacity(0.6) : .dirtyWhite)
    }
}

struct Tertiary: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(maxWidth: .infinity, minHeight: 48)
            .frame(height: 48)
            .background(configuration.isPressed ? .black.opacity(0.6) : .black)
            .contentShape(Rectangle())
            .font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: 18))
            .foregroundColor(configuration.isPressed ? .white.opacity(0.6) : .white)
            .cornerRadius(14)
    }
}

struct Clear: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: 18))
            .foregroundColor(configuration.isPressed ? .dirtyWhite.opacity(0.6) : .dirtyWhite)
            .modifier(ClearModifier())
    }
}

struct Pressed: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(FontFamily.HelveticaNeue.light.swiftUIFont(size: 16))
            .foregroundColor(.dirtyWhite)
            .padding([.leading, .trailing])
            .background(
                Rectangle()
                    .fill(configuration.isPressed ? Color.secondaryPressed : Color.clear)
                    .frame(width: 80, height: 35))
    }
}

struct TabPressed: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(FontFamily.HelveticaNeue.light.swiftUIFont(size: 16))
            .foregroundColor(.dirtyWhite)
            .background(
                Rectangle()
                    .fill(configuration.isPressed ? Color.secondaryPressed : Color.clear)
                    .frame(width: 96, height: 50))
    }
}

struct PrimaryModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity, minHeight: 48)
            .frame(height: 48)
            .background(.white)
            .contentShape(Rectangle())
    }
}

struct SecondaryModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity)
            .frame(height: 48)
            .background(Color.grey)
    }
}

struct ClearModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity)
            .frame(height: 48)
            .background(Color.clear)
    }
}

extension Text {
    func primary() -> some View {
        bold(size: 18, color: .black)
            .modifier(PrimaryModifier())
    }

    func secondary() -> some View {
        bold(size: 18, color: .dirtyWhite)
            .modifier(SecondaryModifier())
    }
}

struct PrimaryButtonModifier_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Button("Primary") {}
                .buttonStyle(Primary())
            Button("Secondary") {}
                .buttonStyle(Secondary())
        }
        .background(.black)
    }
}
