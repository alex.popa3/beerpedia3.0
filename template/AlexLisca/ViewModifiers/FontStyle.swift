//
//  FontStyle.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 16/03/23.
//  Copyright © 2022 RAWFISH SRL. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
    static let dirtyWhite = Color(red: 244 / 255,
                                  green: 245 / 255,
                                  blue: 244 / 255)
    static let grey = Color(red: 255 / 255,
                            green: 255 / 255,
                            blue: 255 / 255,
                            opacity: 0.3)
    static let lightGrey = Color(red: 151 / 255,
                                 green: 151 / 255,
                                 blue: 151 / 255,
                                 opacity: 1)
    static let darkGrey = Color(red: 32 / 255,
                                green: 32 / 255,
                                blue: 32 / 255,
                                opacity: 1)
    static let secondaryPressed = Color(red: 41 / 255,
                                        green: 41 / 255,
                                        blue: 41 / 255,
                                        opacity: 1)
    static let errorRed = Color(red: 226 / 255,
                                green: 0 / 255,
                                blue: 68 / 255,
                                opacity: 1)
}

extension Text {
    func light(size: CGFloat = 16, color: Color = .black) -> Self {
        font(FontFamily.HelveticaNeue.light.swiftUIFont(size: size))
            .foregroundColor(color)
    }

    func bold(size: CGFloat = 18, color: Color = .black) -> Self {
        font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: size))
            .foregroundColor(color)
    }
    
    func medium(size: CGFloat = 17, color: Color = .black) -> Self {
        font(FontFamily.HelveticaNeue.medium.swiftUIFont(size: size))
            .foregroundColor(color)
    }


    func regular(size: CGFloat = 18, color: Color = .black) -> Self {
        font(FontFamily.HelveticaNeue.regular.swiftUIFont(size: size))
            .foregroundColor(color)
    }
}

extension TextField {
    func light(size: CGFloat = 16, color: Color = .black) -> some View {
        foregroundColor(color)
            .font(FontFamily.HelveticaNeue.light.swiftUIFont(size: size))
    }

    func bold(size: CGFloat = 18, color: Color = .black) -> some View {
        foregroundColor(color)
            .font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: size))
    }
}

extension SecureField {
    func light(size: CGFloat = 16, color: Color = .black) -> some View {
        foregroundColor(color)
            .font(FontFamily.HelveticaNeue.light.swiftUIFont(size: size))
    }

    func bold(size: CGFloat = 18, color: Color = .dirtyWhite) -> some View {
        foregroundColor(color)
            .font(FontFamily.HelveticaNeue.bold.swiftUIFont(size: size))
    }
}
