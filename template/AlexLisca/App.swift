//
//  AppCore.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 17/01/23.
//

import SwiftUI



@main
struct AppCore: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @Environment(\.scenePhase) private var scenePhase
    
    let dependencies: AppDependencies
    let appState: Store<AppState>
    //let systemEventsHandler: SystemEventsHandler
    
   

    init() {
        let appState = Store<AppState>(AppState())
        let session = AppCore.configuredURLSession()
        let webServices = AppCore.configuredWebServices(urlSession: session)
        let databaseServices = AppCore.configuredDatabaseService()
        let interactors = AppCore.configuredInteractors(appState: appState,
                                                        webServices: webServices,
                                                        databaseServices: databaseServices)
        self.appState = appState
        dependencies = AppDependencies(appStateStore: appState,
                                       interactors: interactors)
        
      
    }

    var body: some Scene {
        WindowGroup {
            RootView(rootNode: AppRoute.main,
                     content: { $0.getView() })
                .environment(\.dependencies, dependencies)
                .onOpenURL { url in
                    print("Received URL: \(url)")
                }
               // .onChange(of: scenePhase) { newScenePhase in
                //    systemEventsHandler.handleScene(phase: newScenePhase)
                //}
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_: UIApplication,
                     didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        print("LiscaApp application is starting up. ApplicationDelegate didFinishLaunchingWithOptions.")
        return true
    }
}
