//
//  AppState.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 02/11/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import SwiftUI

class AppState: Equatable {
    var userData = UserData()
    var system = System()
    var favoriteIds = [String]()
    var filters = Filter.initialValues
    var permissions = Permissions()
}

extension AppState {
    struct UserData: Equatable {
        var username: String = ""
    }
}

extension AppState {
    struct System: Equatable {
        var isActive: Bool = false
        var keyboardHeight: CGFloat = 0
    }
}

extension AppState {
    struct Permissions: Equatable {
        var push: Permission.Status = .unknown
    }

    static func permissionKeyPath(for permission: Permission) -> WritableKeyPath<AppState, Permission.Status> {
        let pathToPermissions = \AppState.permissions
        switch permission {
        case .pushNotifications:
            return pathToPermissions.appending(path: \.push)
        }
    }
}

func == (lhs: AppState, rhs: AppState) -> Bool {
    lhs.userData == rhs.userData &&
        lhs.system == rhs.system &&
        lhs.permissions == rhs.permissions
}

#if DEBUG
    extension AppState {
        static var preview: AppState {
            let state = AppState()
            state.system.isActive = true
            return state
        }
    }
#endif
