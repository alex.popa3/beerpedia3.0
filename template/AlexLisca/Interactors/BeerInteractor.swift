//
//  BeerInteractor.swift
//  AlexLisca
//
//  Created by Rawfish on 05/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Combine
import Foundation
import SwiftUI

protocol BeerInteractor {
    func load(beers: LoadableSubject<[Beer]>, offset: Int)
    func reload(beers: LoadableSubject<[Beer]>)
    func load(details: LoadableSubject<Beer.Details>, for beerId: Int)
    func updateFavorite(beers: LoadableSubject<[Beer]>)
    func loadRandom(beer: LoadableSubject<Beer.Details>)
}

class RealBeerInteractor: BeerInteractor {
    let webService: RealBeerWeebService
    let dbService: BeerDatabaseService
    let appState: Store<AppState>
    
    private var cancellables = Set<AnyCancellable>()
    
    init(databaseService: BeerDatabaseService,
         webService: RealBeerWeebService,
         appState: Store<AppState>)
    {
        self.dbService = databaseService
        self.webService = webService
        self.appState = appState
    }
    
    func load(beers: LoadableSubject<[Beer]>, offset: Int) {
        Task(priority: .high) {
            await beers.networkBoundResource(
                query: {
                    let storedBeers = try await self.dbService.beers()
                    return storedBeers
                },
                shouldFetch: { cached in
                    var request: Bool {
                        var result = false
                        if (cached?.count ?? 0 == offset) && (offset == 325) {
                            print("Request denied")
                            result = false
                        } else if (cached?.count ?? 0 <= offset) && (offset >= 0) {
                            print("\(offset / 80)")
                            print("Request acepted")
                            result = true
                        }
                        
                        return result
                    }
                    
                    return request
                },
                fetch: { _ in
                    print("Il data count \(offset)")
                    let page = try await self.webService.getBeers(page: offset / 80 + 1)
                    try await dbService.store(beer: page)
                    return try await dbService.beers()
                    
                },
                saveFetch: nil)
        }
    }
    
    func loadRandom(beer: LoadableSubject<Beer.Details>) {
     
        Task(priority: .low) {
            await beer.networkBoundResource(
                query: { nil },
                fetch: { _ in
                    try await webService.getRandomBeer()
                        .first ?? .mockedData[0]
                    
                }, saveFetch: { data in
                try await self.dbService.store(beer: data)
            })
        }
    }
    
    func reload(beers: LoadableSubject<[Beer]>) {
        Task(priority: .background) {
            await beers.networkBoundResource(
                query: {
                    try await self.dbService.clearBeers()
                    return nil
                },
                fetch: { _ in
                    try await webService.getBeers(page: 1)
                    
                },
                saveFetch: { data in
                    try await self.dbService.store(beer: data)
                })
        }
    }
    
    func load(details: LoadableSubject<Beer.Details>, for beerId: Int) {
        Task(priority: .background) {
            await details.networkBoundResource(
                query: {
                    try await self.dbService.details(for:String(beerId))
                },
                fetch: { _ in
                    try await webService.getDetails(with: beerId)
                        .first ?? Beer.Details.mockedData[0]
                    
                },
                saveFetch:{ data in
                    try await self.dbService.store(beer: data)
                })
        }
    }
    
    func updateFavorite(beers: LoadableSubject<[Beer]>) {
        beers.wrappedValue = .isLoading(last: nil)
        appState
            .updates(for: \.favoriteIds)
            .flatMap { ids in
                ids.publisher
                    .await { id in try? await self.dbService.beer(with: String(id)) }
                    .collect()
            }
            .map { $0.compactMap { $0 } }
            .sink(receiveValue: { values in
                beers.wrappedValue = .loaded(values)
            })
            .store(in: &cancellables)
    }
}

struct StubBeerInteractor: BeerInteractor {
    
    func load(beers: LoadableSubject<[Beer]>, offset: Int) {
        beers.wrappedValue = .loaded(Beer.mockedData)
    }
    
    func clearDb() {}
    
    func reload(beers: LoadableSubject<[Beer]>) {
        beers.wrappedValue = .loaded(Beer.mockedData)
    }
    
    func load(details: LoadableSubject<Beer.Details>, for beerId: Int) {
        details.wrappedValue = .loaded(Beer.Details.mockedData[0])
    }
    
    func updateFavorite(beers: LoadableSubject<[Beer]>) {}
    
    func loadRandom(beer: LoadableSubject<Beer.Details>) {}
}
