//
//  FavoritesInteractor.swift
//  AlexLisca
//
//  Created by Rawfish on 10/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation
import UIKit


protocol FavoritesInteractor {
    func toggleFavorite(with id: String)
    func isFavorite(id: String) -> Bool
}


class RealFavoritesInteractor: FavoritesInteractor {
    
    let appState: Store<AppState>
    let defaultService: UserDefaultsService
    
    init(appState: Store<AppState>, defaultService: UserDefaultsService) {
        self.appState = appState
        self.defaultService = defaultService
        let favoritesIds: [String]? = defaultService.value(for: .favoritesIds)
        appState[\.favoriteIds] = favoritesIds ?? []
    }
    
    func toggleFavorite(with id: String) {
        var favorites = appState[\.favoriteIds]
        if let favoriteIndex = appState[\.favoriteIds].firstIndex(where: { $0 == id }) {
            favorites.remove(at: favoriteIndex)
            appState[\.favoriteIds] = favorites
            defaultService.set(value: favorites, for: .favoritesIds)
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.warning)
            return
        }
        favorites.append(id)
        appState[\.favoriteIds] = favorites
        defaultService.set(value: favorites, for: .favoritesIds)
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        
    }
    
    func isFavorite(id: String) -> Bool {
        appState[\.favoriteIds].contains( where:  { $0 == id})
    }
}


class StubFavoritesInteractor: FavoritesInteractor {
    func toggleFavorite(with id: String) {
        
    }
    
    func isFavorite(id: String) -> Bool {
        true
    }
}
