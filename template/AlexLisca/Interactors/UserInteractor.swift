//
//  UserInteractor.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 16/03/23.
//

import Combine
import Foundation
import SwiftUI

protocol UserInteractor {
    func shouldShowWizard() -> Bool
    func wizardCompleted()
}

class RealUserInteractor: UserInteractor {
    let defaultsService: UserDefaultsService
    let appState: Store<AppState>

    private var cancellables = Set<AnyCancellable>()

    init(defaultsService: UserDefaultsService,
         appState: Store<AppState>) {
        self.defaultsService = defaultsService
        self.appState = appState
    }

    func shouldShowWizard() -> Bool {
        defaultsService.value(for: .shouldShowWizard) ?? true
    }

    func wizardCompleted() {
        defaultsService.set(value: false, for: .shouldShowWizard)
    }
}

struct StubUserInteractor: UserInteractor {
    func shouldShowWizard() -> Bool {
        true
    }

    func wizardCompleted() {}
}
