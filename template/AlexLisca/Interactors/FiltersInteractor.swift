//
//  FiltersInteractor.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation

protocol FiltersInteractor {
    func updateFilter(with id: Int, with option: Int)
    func resetFilters()
}

class RealFiltersInteractor: FiltersInteractor {
    let appState: Store<AppState>

    init(appState: Store<AppState>) {
        self.appState = appState
    }

    func updateFilter(with id: Int, with option: Int) {
        guard let filterIndex = appState[\.filters].firstIndex(where: { $0.id == id }) else { return }
        var filter = appState[\.filters][filterIndex]
        guard let optionIndex = filter.values.firstIndex(where: { $0.id == option }) else { return }
        filter.values[optionIndex].isSelected.toggle()
        appState[\.filters][filterIndex] = filter
    }

    func resetFilters() {
        appState[\.filters] = Filter.initialValues
    }
}

class StubFiltersInteractor: FiltersInteractor {
    func resetFilters() {}

    func updateFilter(with _: Int, with _: Int) {}
}
