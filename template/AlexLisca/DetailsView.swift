//
//  DetailsView.swift
//  AlexLisca
//
//  Created by Rawfish on 08/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import SwiftUI

struct DetailsView: View {
    @Environment(\.dependencies) private var dependencies: AppDependencies
    @EnvironmentObject var router: Router<AppRoute>
    
    let beer: Int
    @State private var details: Loadable<Beer.Details>
    @State var isFavorite: Bool
    let isRandom: Bool
    
    init(beer: Int, details: Loadable<Beer.Details> = .notRequested, isFavorites:Bool, isRandom: Bool) {
        self.beer =  beer
        _isFavorite = .init(initialValue: false)
        _details = .init(initialValue: details)
        self.isRandom = isRandom
        let navBarApp = UINavigationBarAppearance()
        navBarApp.configureWithTransparentBackground()
        UINavigationBar.appearance().scrollEdgeAppearance = navBarApp
        UINavigationBar.appearance().standardAppearance = navBarApp
        
       
    }

    var body: some View {
        content
            .navigationBarTitle("")
    }
    
    @ViewBuilder
    private var content: some View {
        switch details {
        case .notRequested: notRequestedView
        case let .isLoading(last: details):
            if let details = details {
                loadedView(details)
            } else {
                loadingView
            }
        case let .loaded(details): loadedView(details)
        case let .failed(error): failedView(error)
        }
    }
}

private extension DetailsView {
    var notRequestedView: some View {
        Text("").onAppear {
            self.loadDetails()
        }
    }
    
    var loadingView: some View {
        VStack {
            ActivityIndicatorView()
            Button(action: { self.details.cancelLoading() },
                   label: { Text(L10n.Common.cancel) })
        }
    }
    
    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: { self.loadDetails() })
    }
    
    func loadedView(_ details: Beer.Details) -> some View {
        ScrollView(showsIndicators: false) {
            VStack(alignment: .center) {
                VStack {
                    AsyncImage(
                        url: URL(string: details.image_url ?? ""),
                        content: { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 250, height: 350)
                                .shadow(color: .black.opacity(0.25), radius: 200)
                                .animation(.easeInOut(duration: 2.0))
                            
                        },
                        placeholder: {
                            if details.image_url != "no Image" && details.image_url != nil {
                                ProgressView()
                            } else {
                                VStack {
                                    Image(systemName: "photo.on.rectangle")
                                        .resizable()
                                        .frame(maxWidth: 100, maxHeight: 85)
                                        .padding(.bottom, 10)
                                    Text("No image to load")
                                        .font(.system(size: 11))
                                }
                                .opacity(0.75)
                                .shadow(radius: 40)
                                .frame(height: 150)
                            }
                        }
                    )
                   
                    Text("Contributed by " + (details.contributed_by ?? ""))
                        .regular(size: 11, color: .gray.opacity(0.70))
                        .padding(.top, 10)
                }
                .padding(.top, 60)
                .padding(.bottom, 20)
                .frame(maxWidth: .infinity)
                .background(.regularMaterial)
                .padding([.leading, .trailing, .top], 1)
                .shadow(color: .black.opacity(0.25), radius: 4, y: 2.5)
                
                HStack {
                    VStack(alignment: .leading) {
                        Text(details.name)
                            .bold(size: 26)
                            .padding(.top, 10)
                            .padding(.bottom, 1)
                            .padding(.leading, 16)
                        Text(details.tagline)
                            .medium(size: 15)
                            .padding(.leading, 16)
                    }
                    Spacer()
                    
                    
                    
                    Button {
                        dependencies.interactors.favoritesInteractor.toggleFavorite(with: String(details.id))
                                                                                    
                        } label: {
                            Image(systemName:  isFavorite == true  ? "star.fill" : "star")
                            .resizable()
                            .foregroundColor(.yellow.opacity(0.50))
                            .frame(width: 30, height: 30)
                            .padding(.trailing, 20)
                    }
                }
                Text(details.description)
                    .regular(size: 17)
                    .padding(.top, 24)
                    .padding(.leading, 16)
                Spacer()
                
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        Button(action: { router.push(.detailSheet(details, false, true, false)) }, label: { Text("Ingredients") })
                            .secondaryButton()
                          
                        Button(action: {
                            router.push(.detailSheet(details, true, false, false))
                        }, label: { Text("Method") })
                            .secondaryButton()
                          
                        Button(action: {
                            router.push(.detailSheet(details, false, false, true))
                        }, label: { Text("Values") })
                            .secondaryButton()
                    }
                    .padding(15)
                }
                
                VStack(alignment: .leading) {
                    Text("Foods Pairing")
                        .bold(size: 26)
                        .padding(.bottom, 2)
                        .padding(.leading, 15)
                    ScrollView(.horizontal,showsIndicators: false) {
                        HStack {
                            ForEach(details.food_pairing ?? [""], id: \.self) {
                                Text($0.capitalized)
                                    .regular(size: 16)
                                    .secondaryButton()
                            }
                        }
                        .padding(.leading, 15)
                    }
                }
                .padding(.bottom,20)
                
                Spacer()
            }
            
            .frame(maxHeight: .infinity)
            .background()
        } 
        .onReceive(dependencies.appState.map(\.favoriteIds)) { favorites in
            isFavorite = favorites.contains(where: { $0 == String(details.id) })
        }
        .background()
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        
    }
}

private extension DetailsView {
    func loadDetails() {
        
        if isRandom == false {
            dependencies.interactors.beerInteractor
                .load(details: $details, for: beer)
        } else if isRandom == true {
            print("is random")
            dependencies.interactors.beerInteractor.loadRandom(beer: $details)
        }
        
    }
}

private enum DetailsViewModifiers {
    struct SecondaryButton: ViewModifier {
        func body(content: Content) -> some View {
            content
                .padding([.top, .bottom], 10)
                .padding([.leading, .trailing], 15)
                .background(.regularMaterial)
                .cornerRadius(20)
        }
    }
}

private extension View {
    func secondaryButton() -> some View {
        modifier(DetailsViewModifiers.SecondaryButton())
    }
}

#if DEBUG

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            DetailsView(beer: Beer.mockedData[0].id, isFavorites: false,isRandom: false)
                .inject(.preview)
        }
    }
}
#endif
