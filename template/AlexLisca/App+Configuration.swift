//
//  LiscaApp+Configuration.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 18/01/23.
//

import SwiftUI

extension AppCore {
    static func configuredURLSession() -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 120
        configuration.waitsForConnectivity = true
        configuration.httpMaximumConnectionsPerHost = 5
        configuration.requestCachePolicy = .reloadRevalidatingCacheData
        configuration.urlCache = .shared
        return URLSession(configuration: configuration)
    }

    static func configuredWebServices(urlSession : URLSession) -> AppDependencies.WebServices {
        guard let plist: Plist = try? Plist<InfoPlist>() else {
            fatalError("ERROR: Error initializing networking configuration.")
        }
        let url = plist.data.configuration.networking.baseUrl
        let customWebService = RealBeerWeebService(session: urlSession,
                                                   baseUrl: url)
        return .init(customWebService: customWebService)
      //  return .init()
    }

    static func configuredDatabaseService() -> AppDependencies.DatabaseServices {
        .init(userDefaultsService: StandardUserDefaultsService(),
        customDatabaseService: BeerCoreDataService(version: 1))
    }

    static func configuredInteractors(appState: Store<AppState>,
                                      webServices : AppDependencies.WebServices,
                                      databaseServices: AppDependencies.DatabaseServices) -> AppDependencies.Interactors {
        let userPermissionsInteractor = RealUserPermissionsInteractor(appState: appState,
                                                                      openAppSettings: {
                                                                          URL(string: UIApplication.openSettingsURLString).flatMap {
                                                                              UIApplication.shared.open($0, options: [:], completionHandler: nil)
                                                                          }
                                                                      })
            let customInteractor = RealBeerInteractor(databaseService: databaseServices.customDatabaseService,
                                                        webService: webServices.customWebService,
                                                        appState: appState)
        let userInteractor = RealUserInteractor(defaultsService: databaseServices.userDefaultsService,
                                                appState: appState)
        let filterInteractor = RealFiltersInteractor(appState: appState)
        let favoritesInteractor = RealFavoritesInteractor(appState: appState, defaultService: databaseServices.userDefaultsService)
        return .init( customInteractor: customInteractor,
            userInteractor: userInteractor,
            userPermissionsInteractor: userPermissionsInteractor,
                      favoritesInteractor: favoritesInteractor,
                      filterInteractor: filterInteractor
        )
    }
}
