//
//  InfiniteGrid.swift
//  soundfit
//
//  Created by Alessandro Maroso on 06/07/22.
//  Copyright © 2022 RAWFISH SRL. All rights reserved.
//

import SwiftUI

struct InfiniteGrid<Data, Content>: View where Data: RandomAccessCollection, Data.Element: Hashable, Content: View {
    @Binding var data: Loadable<Data>
    let loadMore: () -> Void
    let content: (Data.Element) -> Content
    let columns: [GridItem]

    init(data: LoadableSubject<Data>,
         columns: [GridItem] = [GridItem(.adaptive(minimum: 80))],
         loadMore: @escaping () -> Void,
         @ViewBuilder content: @escaping (Data.Element) -> Content) {
        _data = data
        self.columns = columns
        self.loadMore = loadMore
        self.content = content
    }

    var body: some View {
        ScrollView(showsIndicators: false) {
            LazyVGrid(columns: columns) {
                if case let .loaded(items) = data {
                    ForEach(items, id: \.self) { item in
                        content(item)
                            .listRowSeparator(.hidden)
                            .listRowBackground(Color.clear)
                            .onAppear {
                                if item == items.last {
                                    loadMore()
                                }
                            }
                    }
                }
                if case let .isLoading(items) = data {
                    if let items {
                        ForEach(items, id: \.self) { item in
                            content(item)
                                .listRowSeparator(.hidden)
                                .listRowBackground(Color.clear)
                        }
                    }
                    ProgressView()
                        .frame(idealWidth: .infinity, maxWidth: .infinity, alignment: .center)
                        .listRowBackground(Color.clear)
                }
            }
        }
        .background(Color.black)
        .onAppear(perform: loadMore)
    }
}
