//
//  InfiniteList.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 22/12/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import SwiftUI

struct InfiniteList<Data, Content>: View where Data: RandomAccessCollection, Data.Element: Hashable, Content: View {
    @Binding var data: Loadable<Data>
    let loadMore: () -> Void
    let content: (Data.Element) -> Content

    init(data: LoadableSubject<Data>,
         loadMore: @escaping () -> Void,
         @ViewBuilder content: @escaping (Data.Element) -> Content)
    {
        _data = data
        self.loadMore = loadMore
        self.content = content
        UITableView.appearance().separatorStyle = .none
        UITableViewCell.appearance().backgroundColor = .black
        UITableView.appearance().backgroundColor = .black
       
    }

    var body: some View {
        ScrollView {
            LazyVStack {
                
                if case let .loaded(items) = data {
                    renderContent(data: items)
                }
                
                if case let .isLoading(items) = data {
                    if let items {
                        renderContent(data: items)
                    }
                    ProgressView()
                        .frame(idealWidth: .infinity, maxWidth: .infinity, alignment: .center)
                        .listRowBackground(Color.clear)
                }
            }
            .background()
        }.frame(maxWidth: .infinity,maxHeight: .infinity)
    }

    @ViewBuilder
    func renderContent(data: Data) -> some View {
        ForEach(data, id: \.self) { item in
            content(item)
               .onAppear {
                   if item == data.last {
                        loadMore()
                    }
                }
        }
    }
}
