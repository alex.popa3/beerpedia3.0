//
//  ActivityIndicatorView.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 07/12/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import SwiftUI

struct ActivityIndicatorView: UIViewRepresentable {
    func makeUIView(context _: UIViewRepresentableContext<ActivityIndicatorView>) -> UIActivityIndicatorView {
        UIActivityIndicatorView(style: .large)
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context _: UIViewRepresentableContext<ActivityIndicatorView>) {
        uiView.startAnimating()
    }
}
