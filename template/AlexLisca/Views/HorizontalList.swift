//
//  HorizontalList.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 21/06/22.
//  Copyright © 2022 Rawfish. All rights reserved.
//

import SwiftUI

struct HorizontalList<Data, Content>: View where Data: RandomAccessCollection, Data.Element: Hashable, Content: View {
    @Binding var data: Loadable<Data>
    let loadMore: () -> Void
    let content: (Data.Element) -> Content
    let padding: CGFloat

    init(data: LoadableSubject<Data>,
         loadMore: @escaping () -> Void,
         @ViewBuilder content: @escaping (Data.Element) -> Content,
         padding: CGFloat = 16) {
        _data = data
        self.loadMore = loadMore
        self.content = content
        self.padding = padding
    }

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                if case let .loaded(items) = data {
                    ForEach(items, id: \.self) { item in
                        content(item)
                            .onAppear {
                                if item == items.last {
                                    loadMore()
                                }
                            }
                    }
                } else if case let .isLoading(items) = data {
                    if let items {
                        ForEach(items, id: \.self) { item in
                            content(item)
                        }
                    }
                    ActivityIndicatorView()
                        .frame(width: UIScreen.main.bounds.width)
                }
            }
            .padding(EdgeInsets(top: 0,
                                leading: 16,
                                bottom: 0,
                                trailing: 16))
        }
    }
}
