//
//  ErrorView.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 07/12/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import SwiftUI

struct ErrorView: View {
    let error: Error
    let retryAction: () -> Void

    var body: some View {
        VStack {
            Text("Error")
                .font(.title)
            Text(error.localizedDescription)
                .font(.callout)
                .multilineTextAlignment(.center)
                .padding(.bottom, 40).padding()
            Button(action: retryAction, label: { Text("Retry").bold() })
        }
    }
}

#if DEBUG
    struct ErrorView_Previews: PreviewProvider {
        static var previews: some View {
            ErrorView(error: NSError(domain: "", code: 0, userInfo: [
                NSLocalizedDescriptionKey: "Something went wrong"
            ]),
            retryAction: {})
        }
    }
#endif
