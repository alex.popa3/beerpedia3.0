//
//  FiltersView.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation
import SwiftUI

struct FiltersView: View {
    @EnvironmentObject var router: Router<AppRoute>
    @Environment(\.dependencies) private var dependencies: AppDependencies

    @State private var filters = [Filter]()

    var body: some View {
        VStack(spacing: 0) {
            HStack {
                Spacer()
                Button {
                    router.dismiss()
                } label: {
                    Image(systemName: "multiply")
                }
                .padding()
            }
            .frame(height: 56.0)
            HStack {
                Text("Filtra per")
                    .bold(size: 24)
                Spacer()
            }
            .frame(height: 47.0)
            .padding(.bottom, 18)
            ScrollView(showsIndicators: false) {
                LazyVStack(alignment: .leading, spacing: 24) {
                    calendarView
                    filterViews
                }
            }
            HStack(spacing: 24) {
                Button("Reset") {
                    dependencies.interactors.filterInteractor.resetFilters()
                }
            }
            .padding(.top, 24)
        }
        .padding(.horizontal, 24)
        .onReceive(dependencies.appState.map(\.filters),
                   perform: { filters in
                       self.filters = filters
                   })
    }

    @ViewBuilder
    private var calendarView: some View {
        VStack(alignment: .leading, spacing: 8.0) {
            Text("Period")
                .regular(size: 18)
            Button {} label: {
                HStack(spacing: 16.0) {
                    Text("Lun 14 Mar - Lun 21 Mar")
                        .regular(size: 18)
                    Spacer()
                    Image("calendar")
                }.padding(16.0)
                    .overlay(RoundedRectangle(cornerRadius: 4.0)
                        .stroke(Color.lightGrey, lineWidth: 1)
                        .frame(height: 56.0)
                    )
            }
            .frame(height: 56.0)
        }
    }

    @ViewBuilder
    private var filterViews: some View {
        ForEach(filters) { filter in
            LazyVStack(alignment: .leading, spacing: 10.0) {
                Text(filter.name)
                    .bold(size: 18)
                FlexibleView(availableWidth: UIScreen.main.bounds.width - 30.0,
                             data: filter.values,
                             spacing: 8.0,
                             alignment: .leading) { option in
                    Button(option.name) {
                        dependencies.interactors.filterInteractor
                            .updateFilter(with: filter.id,
                                          with: option.id)
                    }
                    .tint(option.isSelected ? .red : .blue)
                }
            }
        }
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FiltersView()
            .inject(.preview)
    }
}
