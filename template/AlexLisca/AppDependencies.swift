//
//  AppDependencies.swift
//  lisca-swiftui
//
//  Created by Alessandro Maroso on 02/11/21.
//  Copyright © 2021 Rawfish. All rights reserved.
//

import SwiftUI

// MARK: - AppDependencies

struct AppDependencies: EnvironmentKey {
    let appState: Store<AppState>
    let interactors: Interactors

    init(appStateStore: Store<AppState>,
         interactors: Interactors) {
        appState = appStateStore
        self.interactors = interactors
    }

    init(appState: AppState,
         interactors: Interactors) {
        self.appState = Store<AppState>(appState)
        self.interactors = interactors
    }

    static var defaultValue: Self { Self.default }

    private static let `default` = Self(appState: AppState(),
                                        interactors: .stub)
}

extension EnvironmentValues {
    var dependencies: AppDependencies {
        get { self[AppDependencies.self] }
        set { self[AppDependencies.self] = newValue }
    }
}

#if DEBUG
    extension AppDependencies {
        static var preview: Self {
            .init(appState: AppState.preview,
                  interactors: .stub)
        }
    }
#endif

// MARK: - Injection in the view hierarchy

extension View {
    func inject(_ appState: AppState,
                _ interactors: AppDependencies.Interactors) -> some View {
        let container = AppDependencies(appState: appState,
                                        interactors: interactors)
        return inject(container)
    }

    func inject(_ container: AppDependencies) -> some View {
        environment(\.dependencies, container)
    }
}

extension AppDependencies {
    struct WebServices {
        let customWebService: RealBeerWeebService
    }

    struct DatabaseServices {
        let userDefaultsService: UserDefaultsService
        let customDatabaseService: BeerDatabaseService
    }

    struct Interactors {
        let beerInteractor: BeerInteractor
        let userInteractor: UserInteractor
        let userPermissionsInteractor: UserPermissionsInteractor
        let favoritesInteractor: FavoritesInteractor
        let filterInteractor: FiltersInteractor

        init(customInteractor: BeerInteractor,
            userInteractor: UserInteractor,
            userPermissionsInteractor: UserPermissionsInteractor,
             favoritesInteractor: FavoritesInteractor,
             filterInteractor: FiltersInteractor
        ) {
            self.beerInteractor = customInteractor
            self.userInteractor = userInteractor
            self.userPermissionsInteractor = userPermissionsInteractor
            self.favoritesInteractor = favoritesInteractor
            self.filterInteractor = filterInteractor
        }

        static var stub: Self {
            .init( customInteractor: StubBeerInteractor(),
                userInteractor: StubUserInteractor(),
                userPermissionsInteractor: StubUserPermissionsInteractor(),
                   favoritesInteractor: StubFavoritesInteractor(),
                   filterInteractor: StubFiltersInteractor()
            )
        }
    }
}
