//
//  DetailsSheet.swift
//  AlexLisca
//
//  Created by Rawfish on 10/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import SwiftUI

struct DetailsSheet: View {
    let beer: Beer.Details
    let method: Bool
    let ingredients: Bool
    let values: Bool
    
    var body: some View {
        if method == true {
            VStack {
                if beer.method.fermentation.unit?.isEmpty == true {
                    Text("NO DATA ")
                        
                }
                ForEach(beer.method.mash_temp, id: \.self) { mash in
                    HStack {
                        Text("Duration:")
                        Text(String(format: "%g", mash.duration ?? 0.0))
                            .medium()
                        Spacer()
                        Text(String(mash.temp.value ?? 0.0))
                            .medium()
                        Text(mash.temp.unit ?? "")
                    }
                    .padding([.leading, .trailing], 20)
                }
                .onAppear { print("I DATI IN ON APPEAR\(beer.method)")}
                Spacer()
            }.navigationBarTitle("Method")
                .navigationBarTitleDisplayMode(.inline)

        }
        
        if ingredients == true {
            ScrollView(showsIndicators: false) {
                
                VStack {
                    Text("Hops")
                        .bold(size: 22)
                    ForEach(beer.ingredients.hops, id: \.self) { hop in
                        HStack(alignment: .top) {
                            VStack(alignment: .leading) {
                                Text(hop.name ?? "")
                                HStack {
                                    Text(String(format: "%g", hop.amount.value!))
                                    Text(hop.amount.unit ?? "")
                                }
                            }
                            Spacer()
                            VStack(alignment: .trailing) {
                                Text("Add: " + (hop.add ?? ""))
                                Text("Attribute: " + (hop.attribute ?? ""))
                            }
                        }
                        .padding(20)
                        .background(.regularMaterial)
                        .cornerRadius(30)
                        .padding(10)
                    }
                }
                
                VStack {
                    Text("Malt")
                        .bold(size: 22)
                    .padding([.leading,.trailing],40)
                   
                    ForEach(beer.ingredients.malt, id: \.self) { malt in
                        HStack(alignment: .top) {
                            Text(malt.name ?? "")
                            Spacer()
                            HStack {
                                Text(String(format: "%g", malt.amount.value!))
                                Text(malt.amount.unit ?? "")
                            }
                        }
                        .padding(20)
                        .background(.regularMaterial)
                        .cornerRadius(30)
                        .padding(10)
                    }
                }.navigationBarTitle("Ingredients")
                    .navigationBarTitleDisplayMode(.inline)
                Spacer()
            }
        }
        
        if values == true {
            VStack {
                ValueLine(title: "Abv", value: beer.abv ?? 0.0)
                ValueLine(title: "Ibu", value: beer.ibu ?? 0.0)
                ValueLine(title: "TargetFg", value: beer.target_fg ?? 0.0)
                ValueLine(title: "TargetOg", value: beer.target_og ?? 0.0)
                ValueLine(title: "Ebc", value: beer.ebc ?? 0.0)
                ValueLine(title: "Srm", value: beer.srm ?? 0.0)
                ValueLine(title: "Ph", value: beer.ph ?? 0.0)
                Spacer()
            }.navigationBarTitle("\(beer.name)'s values")
                .navigationBarTitleDisplayMode(.inline)

        }
    }
}

struct DetailsSheet_Previews: PreviewProvider {
    static var previews: some View {
        DetailsSheet(beer: Beer.Details.mockedData[0], method: false, ingredients: true, values: false)
            .inject(.preview)
    }
}

struct ValueLine: View {
    let title: String
    let value: Double
    var body: some View {
        HStack {
            Text(title)
                .bold(size: 16)
            Spacer()
            Text("\(String(format: "%g", value))")
        }
        .padding([.leading, .trailing], 20)
    }
}
