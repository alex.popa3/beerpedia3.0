
#  test iOS

> This project has been developed using the iOS frontend of [lisca Application Starter Kit](https://gitlab.com/rawfish/lisca/lisca-ios)

<#description project#>


## Index

1.  [Team](#1-team)
2.  [Compatibility](#2-compatibility)
3.  [Setup](#3-setup)
4.  [Scheme](#4-scheme)
5.  [Api](#5-api)
6.  [Admin Panel](#6-admin-panel)
7.  [Deployment](#7-deployment)
8.  [Note](#8-note)


## 1. Team

Role | Persons
--- | ---
Project Manager |  <#Project Managers#>
iOS Developer |  <#iOS Developers#>
Android Developer| <#Android Developers#>
Backend Developer |  <#Backend Developers#>
Designer | <#Designers#>

## 2. Compatibility

Name | Version | Note
--- | --- | ---
Swift | <#Swift Version#> | 
XCode | <#Xcode Version#> |
Min version iOS | <#min Version iOS#> |

Description | Values | Note
--- | --- | ---
Supported platforms | <#platforms#> |
Device orientations | <#orientations#> |

## 3. Setup

Clone the repository and run the following commands

Command | Description | Note
--- | --- | ---
`>` xcode-select --install | Install [Xcode Command Line Tools](https://developer.apple.com/downloads/more) | 
`>` gem install bundler | Install Bundler | 
`>` bundle install | Install Ruby dependencies | 
`>` brew install xcodegen | Install Xcode Gen | 
`>` xcodegen generate | Generate project | 
`>` bundle exec fastlane match development | (Optional) setup fastlane match. | 
`>` bundle exec fastlane match appstore | (Optional) setup fastlane match. | 

> For more information about `bundle exec` open [Using a Gemfile on cocoapods.org](https://guides.cocoapods.org/using/a-gemfile.html). 

> For more information about `match` open [fastlane match git repo](https://docs.fastlane.tools/actions/match/).

> For more information about `XcodeGen` open [xcodegen git repo](https://github.com/yonaskolb/XcodeGen).

## 4. Scheme 

Scheme | Description | Note
--- | --- | ---
Develop | Developing |
Stage | Pre production | 
Production | Release |

> For more information on what a scheme is, check out the apple documentation reference [here](https://developer.apple.com/library/archive/featuredarticles/XcodeConcepts/Concept-Schemes.html).


## 5. Api

>  Check out the api documentation reference [here](<#url swagger#>)

Environment | BaseUrl | Note
--- | --- | ---
Develop | <#baseUrl develop#> | <#note develop#>
Stage | <#baseUrl stage#> | <#note stage#>
Production | <#baseUrl prod#> | <#note prod#>


## 6. Admin Panel

Environment | Note
--- | ---
[Develop](<#url develop#>) |  <#note develop#>
[Stage](<#url stage#>) | <#note stage#>
[Production](<#url prod#>) | <#note prod#>


## 7. Deployment

To deploy a build to App Store Connect (TestFlight and App Store) use the `release` lane. Navigate to your project folder and run:
```script
> bundle exec fastlane release version_number:{the version we are developing} username:{your@apple.id} from_commit{commit id of the previous tag} build_tag:{current tag}
```

> For more information about fastlane open [documentation](https://docs.fastlane.tools).


## 8. Note
<#Note#>






