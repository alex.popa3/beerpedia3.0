//
//  SessionManager.swift
//  AlexLisca
//
//  Created by Rawfish on 11/05/23.
//  Copyright © 2023 RAWFISH SRL. All rights reserved.
//

import Foundation


enum UserState: Equatable {
    case undefined
    case logged(User)
    case notLogged

    static func == (lhs: UserState, rhs: UserState) -> Bool {
        switch (lhs, rhs) {
        case (.undefined, .undefined): return true
        case let (.logged(lhsV), .logged(rhsV)): return lhsV.id == rhsV.id
        case (.notLogged, .notLogged): return true
        default: return false
        }
    }
}


protocol User {
    var id: String { get }
    var email: String? { get }
    var username: String? { get }
    // TODO: to complete with other field
}


enum SessionError: Error {
    case invalidEmail
    case wrongCredential
    case weakPassword
    case emailAlreadyInUse
    case cancelledByUser
    case userNotLogged
    case accountExistsWithDifferentCredential
    case operationNotAllowed
    case generic
}
